<!DOCTYPE>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Desflechados by HOZT</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="style.css">
	<meta property="og:image" content="http://desflechados.com.ve/shareisgood.jpg">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="628">
    <meta property="og:url" content="http://desflechados.com.ve/">
    <meta property="og:title" content="Desflechados - by Hozt & Friends">
    <meta property="og:description" content="¡En Hozt celebramos el mes del amor a nuestro estilo! San Valentin no ha terminado, porque la competencia apenas comienza con ‪#‎DESFLECHADOS‬, un juego hecho en casa, ilustrado por Génesis Quintero, Nubia Navarro y Mariangel Briceño.">
    <meta property="description" content="¡En Hozt celebramos el mes del amor a nuestro estilo! San Valentin no ha terminado, porque la competencia apenas comienza con ‪#‎DESFLECHADOS‬, un juego hecho en casa, ilustrado por Génesis Quintero, Nubia Navarro y Mariangel Briceño."> 
</head>
<body>
	<div id="main" class="container">
		<section id="logo">
		<h1>15/02/2016</h1>
			<a href="../"><img src="imx/logo.png" alt="Desflechados"></a>
		</section>
		
		<footer>
			<a href="http://hozt.com.ve" target="_blank"><img src="imx/byhozt.png" alt=""></a>
		</footer>


	</div>
	<div class="cloudlayer">
		<div class="x1">
			<div class="cloudb"></div>
		</div>

		<div class="x2">
			<div class="clouda"></div>
		</div>

		<div class="x3">
			<div class="cloudb"></div>
		</div>

		<div class="x4">
			<div class="clouda"></div>
		</div>

		<div class="x5">
			<div class="cloudb"></div>
		</div>
	</div>
	<div class="ovnilayer">
		<div class="reverse1">
			<div class="ovni"></div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" ></script>
</body>
</html>