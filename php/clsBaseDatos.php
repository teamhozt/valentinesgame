<?php
/*------------------------------------------------------------------------------
** File:		clsBaseDatos.php
** Class:       BaseDatos
** Description:	Clase para el manejo de base de datos con MySQLi
** Version:		1.5
** Updated:     03-Jan-2015
** Author:		Darwin Jimenez
**------------------------------------------------------------------------------ */
require_once('phpmailer/class.phpmailer.php');

class BaseDatos
{
    var $link;
    var $filter;
    var $HOST;                      // DB HOST
    var $USERNAME;                  // USERNAME
    var $PASSWORD;                  // USER PASSWORD
    var $DBNAME;                    // DB NAME

    var $dbh;
    var $error;
    var $stmt;                      // QUERY STATEMENT

    /**
     * Permite a la clase enviar un mensaje de alerta de error a los administradores
     * para las paginas en produccion
     */
    public function log_db_errors( $error, $query, $severity )
    {
        
        $errors_to = 'dar.jimenez93@gmail.com';
        $debug = true;

        $mail = new PHPMailer();

        $body = '<p>Ha ocurrido un error:</p>';
        $body .= '<p>Fecha/hora del Error '. date('Y-m-d H:i:s').': ';
        $body .= 'Query: '. htmlentities( $query ).'<br />';
        $body .= '</p>';
        $body .= '<p>Severity: '. $severity .'</p>';

        $mail->SetFrom('info@hozt.com.ve', 'Error, dicen');

        $mail->Subject    = "Alerta de Error en la base de datos";

        $mail->MsgHTML($body);

        $address = "dar.jimenez93@gmail.com";
        $mail->AddAddress($address, "Darwin Jimenez");

        $mail->Send();

        if( $debug )
            echo $body;


    }
    
    
	public function __construct()
	{
        $this->HOST     = "localhost";
        $this->USERNAME = "root";
        $this->PASSWORD = "";
        $this->DBNAME = "valentines";

        try {
                $this->link =  new mysqli($this->HOST, $this->USERNAME, $this->PASSWORD, $this->DBNAME);
                mysqli_set_charset($this->link,"utf8");
            } catch (Exception $e) {

                $this->error = $e->getMessage();

            }       
	}
	
	public function __destruct()
	{
		$this->disconnect();
	}
	
	
	/**
     * Filtra los datos de los usuarios
     *
     * @access public
     * @param string, array
     * @return string, array
     *
     */
    public function filter( $data )
    {
        if( !is_array( $data ) )
        {
            $data = trim( htmlentities( $data ) );
        	$data = mysqli_real_escape_string( $this->link, $data );
        }
        else
        {
            $data = array_map( array( 'DB', 'filter' ), $data );
        }
    	return $data;
    }
    
    
    /**
     * Determina si campos comunes no encpsulados estan siendo usados
     *
     * @access public
     * @param string
     * @param array
     * @return bool
     *
     */
    public function db_common( $value = '' )
    {
        if( is_array( $value ) )
        {
            foreach( $value as $v )
            {
                if( preg_match( '/AES_DECRYPT/i', $v ) || preg_match( '/AES_ENCRYPT/i', $v ) || preg_match( '/now()/i', $v ) )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            if( preg_match( '/AES_DECRYPT/i', $value ) || preg_match( '/AES_ENCRYPT/i', $value ) || preg_match( '/now()/i', $value ) )
            {
                return true;
            }
        }
    }
    
    
    /**
     * Ejecuta consultas
     * Todas las funciones siguen se ejecutan a traves de esta funcion
     * Toda la data que pasa por esta funcion, esta automaticamente filtrada con la funcion de filtro
     *
     * @access public
     * @param string
     * @return string
     * @return array
     * @return bool
     *
     */
    public function query( $query )
    {
        $query = $this->link->query( $query );
        if( mysqli_error( $this->link ) )
        {
            $this->log_db_errors( mysqli_error( $this->link ), $query, 'Fatal' );
            return false; 
        }
        else
        {
            return true;
        }
        mysqli_free_result( $query );
    }
    
    
    /**
     * Determina si la tabla existe
     *
     * @access public
     * @param string
     * @return bool
     *
     */
    public function table_exists( $name )
    {
        $check = $this->link->query("SELECT * FROM '$name' LIMIT 1");
        if( $check ) 
        {
            return true;
        }
        else
        {
            return false;
        }
        mysqli_free_result( $check );
    }
    
    
    /**
     * Cuenta la cantidad de filas de una consulta especifica
     *
     * @access public
     * @param string
     * @return int
     *
     */
    public function num_rows( $query )
    {
        $query = $this->link->query( $query );
        if( mysqli_error( $this->link ) )
        {
            $this->log_db_errors( mysqli_error( $this->link ), $query, 'Fatal' );
            return mysqli_error( $this->link );
        }
        else
        {
            return mysqli_num_rows( $query );
        }
        mysqli_free_result( $query );
    }
    
    
    /**
     * Ejecuta un chequeo para ver si el valor existe, retorna true or false
     *
     * Ejemplo:
     * $check_user = array(
     *    'user_email' => 'someuser@gmail.com', 
     *    'user_id' => 48
     * );
     * $exists = exists( 'tu_tabla', 'user_id', $check_user );
     *
     * @access public
     * @param string database table name
     * @param string field to check (i.e. 'user_id' or COUNT(user_id))
     * @param array column name => column value to match
     * @return bool
     *
     */
    public function exists( $table = '', $check_val = '', $params = array() )
    {
        if( empty($table) || empty($check_val) || empty($params) )
        {
            return false;
            exit;
        }
        $check = array();
        foreach( $params as $field => $value )
        {
            
            if( !empty( $field ) && !empty( $value ) )
            {
                //Chequea comando usados frecuentemente en MySQL
                if( $this->db_common( $value ) )
                {
                    $check[] = "$field = $value";   
                }
                else
                {
                    $check[] = "$field = '$value'";   
                }
            }

        }
        $check = implode(' AND ', $check);

        $rs_check = "SELECT $check_val FROM ".$table." WHERE $check";
    	$number = $this->num_rows( $rs_check );
        if( $number === 0 )
        {
            return false;
        }
        else
        {
            return true;
        }
        exit;
    }
    
    
    /**
     * Retorna una fila de un query especifico
     *
     * @access public
     * @param string
     * @return array
     *
     */
    public function get_row( $query )
    {
        $query = $this->link->query( $query );
        if( mysqli_error( $this->link ) )
        {
            $this->log_db_errors( mysqli_error( $this->link ), $query, 'Fatal' );
            return false;
        }
        else
        {
            $r = mysqli_fetch_assoc( $query );
            mysqli_free_result( $query );
            return $r;   
        }
    }
    
    
    /**
     * Ejecuta una consulta que retorna un arreglo con los resultados
     *
     * @access public
     * @param string
     * @return array
     *
     */
    public function get_results( $query )
    {
        $row = array();
        $query = $this->link->query( $query );
        if( mysqli_error( $this->link ) )
        {
            $this->log_db_errors( mysqli_error( $this->link ), $query, 'Fatal' );
            return false;
        }
        else
        {
            while( $r = mysqli_fetch_array( $query, MYSQLI_ASSOC ) )
            {
                $row[] = $r;
            }
            mysqli_free_result( $query );
            return $row;   
        }
    }
    
    
    /**
     * Inserta datos en una tabla de la base de datos
     *
     * @access public
     * @param string table name
     * @param array table column => column value
     * @return bool
     *
     */
    public function insert( $table, $variables = array() )
    {
        
        $sql = "INSERT INTO ". $table;
        $fields = array();
        $values = array();
        foreach( $variables as $field => $value )
        {
            $fields[] = $field;
            $values[] = "'".$value."'";
        }
        $fields = ' (' . implode(', ', $fields) . ')';
        $values = '('. implode(', ', $values) .')';
        
        $sql .= $fields .' VALUES '. $values;

        $query = mysqli_query( $this->link, $sql );
        
        if( mysqli_error( $this->link ) )
        {

            $this->log_db_errors( mysqli_error( $this->link ), $sql, 'Fatal' );
            return false;
        }
        else
        {
            return true;
        }
    }
    
    /**
    * Insercion de datos segura en la base de datos
    *
    * @access public
    * @param string table name
    * @param array table column => column value
    * @return bool
    */
    public function insert_safe( $table, $variables = array() )
    {
        $sql = "INSERT INTO ". $table;
        $fields = array();
        $values = array();
        foreach( $variables as $field => $value )
        {
            $fields[] = $this->filter( $field );

            $values[] = $value; 
        }
        $fields = ' (' . implode(', ', $fields) . ')';
        $values = '('. implode(', ', $values) .')';
        
        $sql .= $fields .' VALUES '. $values;
        $query = mysqli_query( $this->link, $sql );
        
        if( mysqli_error( $this->link ) )
        {
            $this->log_db_errors( mysqli_error( $this->link ), $sql, 'Fatal' );
            return false;
        }
        else
        {
            return true;
        }
    }
    
    
    /**
     * Actualizar datos en la tabla de base de datos
     *
     * @access public
     * @param string table name
     * @param array values to update table column => column value
     * @param array where parameters table column => column value
     * @param int limit
     * @return bool
     *
     */
    public function update( $table, $variables = array(), $where = array(), $limit = '' )
    {

        $sql = "UPDATE ". $table ." SET ";
        foreach( $variables as $field => $value )
        {
            
            $updates[] = "`$field` = '$value'";
        }
        $sql .= implode(', ', $updates);
        
        foreach( $where as $field => $value )
        {
            $value = $value;
                
            $clause[] = "$field = '$value'";
        }
        $sql .= ' WHERE '. implode(' AND ', $clause);
        
        if( !empty( $limit ) )
        {
            $sql .= ' LIMIT '. $limit;
        }

        $query = mysqli_query( $this->link, $sql );

        if( mysqli_error( $this->link ) )
        {
            $this->log_db_errors( mysqli_error( $this->link ), $sql, 'Fatal' );
            return false;
        }
        else
        {
            return true;
        }
    }
    
    
    /**
     * Elimina datos en la taba de base de datos
     *
     * @access public
     * @param string table name
     * @param array where parameters table column => column value
     * @param int max number of rows to remove.
     * @return bool
     *
     */
    public function delete( $table, $where = array(), $limit = '' )
    {
        $sql = "DELETE FROM ". $table;
        foreach( $where as $field => $value )
        {
            $value = $value;
            $clause[] = "$field = '$value'";
        }
        $sql .= " WHERE ". implode(' AND ', $clause);
        
        if( !empty( $limit ) )
        {
            $sql .= " LIMIT ". $limit;
        }
            
        $query = mysqli_query( $this->link, $sql );

        if( mysqli_error( $this->link ) )
        {
            //return false; //
            $this->log_db_errors( mysqli_error( $this->link ), $sql, 'Fatal' );
            return false;
        }
        else
        {
            return true;
        }
    }
    
    
    /**
     * Retorna el ID de la ultima insercion de datos
     *
     * @access public
     * @param none
     * @return int
     *
     */
    public function lastid()
    {
        return mysqli_insert_id( $this->link );
    }
    
    
    /**
     * Retorna la cantidad de columnas
     *
     * @access public
     * @param query
     * @return int
     */
    public function num_fields( $query )
    {
        $query = $this->link->query( $query );
        return mysqli_num_fields( $query );
        mysqli_free_result( $query );
    }
    
    /**
     * Retorna los nombres de los campos
     *
     * @access public
     * @param query
     * @return array
     */
    public function list_fields( $query )
    {
        $query = $this->link->query( $query );
        return mysqli_fetch_fields( $query );
        mysqli_free_result( $query );
    }
    
    
    
    /**
     * Trucar tablas completas
     *
     * @access public
     * @param array database table names
     * @return int number of tables truncated
     *
     */
    public function truncate( $tables = array() )
    {
        if( !empty($tables) )
        {
            $truncated = 0;
            foreach( $tables as $table )
            {
                $truncate = "TRUNCATE TABLE `".trim($table)."`";
                mysqli_query( $this->link, $truncate );
                if( !mysqli_error( $this->link ) )
                {
                    $truncated++;
                }
            }
            return $truncated;
        }
    }
    
    
    /**
     * Imprime o devuelva resultados en un formato
     *
     * @access public
     * @param string variable
     * @param bool echo [true,false] defaults to true
     * @return string
     *
     */
    public function display( $variable, $echo = true )
    {
        $out = '';
        if( !is_array( $variable ) )
        {
            $out .= $variable;
        }
        else
        {
            $out .= '<pre>';
            $out .= print_r( $variable, TRUE );
            $out .= '</pre>';
        }
        if( $echo === true )
        {
            echo $out;
        }
        else
        {
            return $out;
        }
    }
    
    
    /**
     * Desconetar del servidor de base de datos
     * Llamado automaticamente desde el _destruct() en la clase
     */
    public function disconnect()
    {
		mysqli_close( $this->link );
	}

}