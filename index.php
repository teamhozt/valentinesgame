<!DOCTYPE>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
		<title>Desflechados by HOZT</title>
		<link rel="icon" href="assets/images/favicon.png" type="image/png">
		<script type="text/javascript" src="js/phaser.js"></script>
		<script type="text/javascript" src="js/phaser-state-transition-plugin.min.js"></script>
		<?php $fecha = new DateTime(); ?>
        <script type="text/javascript" src="js/main.js?fuckcache=<?php echo $fecha->getTimestamp();  ?>"></script>
		<meta property="og:image" content="http://desflechados.com.ve/shareisgood.jpg">
	    <meta property="og:image:type" content="image/jpeg">
	    <meta property="og:image:width" content="1200">
	    <meta property="og:image:height" content="628">
	    <meta property="og:url" content="http://desflechados.com.ve/">
	    <meta property="og:title" content="Desflechados - by Hozt & Friends">
	    <meta property="og:description" content="¡En Hozt celebramos el mes del amor a nuestro estilo! San Valentin no ha terminado, porque la competencia apenas comienza con un juego hecho en casa, ilustrado por Génesis Quintero, Nubia Navarro y Mariangel Briceño.">
	    <meta property="description" content="¡En Hozt celebramos el mes del amor a nuestro estilo! San Valentin no ha terminado, porque la competencia apenas comienza con un juego hecho en casa, ilustrado por Génesis Quintero, Nubia Navarro y Mariangel Briceño."> 
		<style>
		  body {
		    padding: 0px;
		    margin: 0px;
		    background-color: black;
		  }
		</style>
	</head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-10630657-26', 'auto');
  ga('send', 'pageview');

</script>
	<body>
	</body>
</html>
