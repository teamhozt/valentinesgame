var mixins = {
		addMenuOption: function(text, callback,	className){
			
			// usa el argumento que viene de class name, o usa el predefinido en menuConifg
    		// pero si MenuConfig no esta definido, solo usa 'default'

			className || (className = this.menuConfig.className || 'default');

			// Define la cordenada en x a game.world.center, si colocamos la opcion center
			// de otra forma solo usara la cordenada de x definida 
			var x = this.menuConfig.startX === "center" ?
					game.world.centerX :
					this.menuConfig.startX;

			// define la cordenada de Y en base al Menuconfig
			var y = this.menuConfig.startY;

			// Se crea la opcion
			var txt = game.add.text(
				x,
				(this.optionCount * 30) + y,
				text,
				style.navitem[className]);
			


			// Usamos el metodo anchor si el StartX esta definido como "center"
			if(txt.x < window.innerWidth)
			txt.anchor.setTo(this.menuConfig.startX === "center" ? 0.5 : 0.0);
			// Special 
			(function(){
				var old_callback = callback;

				callback = function(){
					if(gameOptions.playSound)
						game.sound.play('click-menuitem');
					var result = old_callback.apply(this,arguments);
					return result
				}
			})();


			//Events listeners
			txt.inputEnabled = true;
			
			txt.events.onInputUp.add(callback);
			txt.events.onInputOver.add(function (target){
				target.setStyle(style.navitem.hover);
				if(gameOptions.playSound)
				game.sound.play('hover-menuitem');
			});

			txt.events.onInputOut.add(function (target){
				target.setStyle(style.navitem[className]);
			});

			//Manual center
			if(txt.x > window.innerWidth)
				txt.x = this.centerSpriteX(txt.width);

			this.optionCount++;
		},

		centerSpriteX: function(width){
			var tamano = window.innerWidth;
			var restante = tamano - width;
			var result = 0;
			if(restante>0){
				result = (restante/2);
			}
			return result;
		}
}