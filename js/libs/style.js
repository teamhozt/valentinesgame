var style;

//this is a wrapped function

(function() {

// the variablesdeclared here will not be scoped anywhere and will only be accessible in this wrapped funcion
	var defaultColor = "#232323",
	highlightColor = "#F65B6B";

	style = {
		header: {
			font: 'bold 60pt JoyStix',
			fill: defaultColor,
			align: 'center'
		},
		navitem: {
			base: {
				font: 'bold 15pt JoyStix',
				align: 'left',
				
			},
			default: {
				fill: defaultColor,	
			},
			inverse: {
				fill: 'white',
				stroke: '#232323',
			},
			hover: {
				fill: highlightColor

			}
		},
		overitem:{
			
		}


	};

	for(var key in style.navitem){
		if(key !== "base"){
			Object.assign(style.navitem[key], style.navitem.base);
		}
	}

})();


// Los parentesis llaman a la funcion de forma inmediata