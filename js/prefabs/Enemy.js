var Platformer = Platformer || {};

Platformer.Enemy = function (game_state, position, properties) {
    "use strict";
    Platformer.Prefab.call(this, game_state, position, properties);
    
    this.walking_speed = +properties.walking_speed;
    this.walking_distance = +properties.walking_distance;
    
    // saving previous x to keep track of walked distance
    this.previous_x = this.x;
    this.previous_y = this.y;
    
    this.game_state.game.physics.arcade.enable(this);

    this.vertical = properties.vertical; // para enemigos con movimiento vertical
    this.fire = properties.fire; // para enemigos que no se pueden matar
    this.radar = properties.radar; // para enemigos que se activan cuando el player este cerca 

    this.realvelocity = properties.direction * this.walking_speed;

    if(this.vertical=="true")
        this.body.velocity.y = this.realvelocity;
    else {
        if(this.radar > 0)
            this.body.velocity.x = 0; // con esto el enemigo no se mueve hasta... (ver update)
        else
            this.body.velocity.x = this.realvelocity;
    }

    
    if(!this.isPlanet)
        this.scale.setTo(-properties.direction, 1);
    
    this.anchor.setTo(0.5);

    this.animations.add("moving", [0, 1], 5, true);
    this.animations.play("moving");
};

Platformer.Enemy.prototype = Object.create(Platformer.Prefab.prototype);
Platformer.Enemy.prototype.constructor = Platformer.Enemy;

Platformer.Enemy.prototype.update = function () {
    "use strict";
    this.game_state.game.physics.arcade.collide(this, this.game_state.layers.collision);
    
    //Esta es la parte magica del radar, lo cual activa el movimiento cuando el player esta cerca
    if((this.radar > 0) && (Phaser.Math.distance(player.x, player.y, this.x, this.y) <= this.radar)) { //DISTANCIA ENTRE PLAYER Y ENEMY
        //console.log(Phaser.Math.distance(player.x, player.y, this.x, this.y));
        this.body.velocity.x = this.realvelocity; 
    }

    if(this.vertical!="true"){
        // change the direction if walked the maximum distance
        if (Math.abs(this.x - this.previous_x) >= this.walking_distance) {
            this.body.velocity.x *= -1;
            this.realvelocity = this.body.velocity.x;
            this.previous_x = this.x;
            if(!this.isPlanet) 
                this.scale.setTo(-this.scale.x, 1);
        }
    }else{
        // change the direction if walked the maximum distance
        if (Math.abs(this.y - this.previous_y) >= this.walking_distance) {
            this.body.velocity.y *= -1;
            this.realvelocity = this.body.velocity.y;
            this.previous_y = this.y;
            if(!this.isPlanet)
                this.scale.setTo(-this.scale.y, 1);
        }
    }




};