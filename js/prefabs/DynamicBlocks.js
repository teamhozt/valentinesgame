var Platformer = Platformer || {};

Platformer.DynamicBlocks = function (game_state, position, properties) {
    "use strict";
    Platformer.Prefab.call(this, game_state, position, properties);
    this.anchor.setTo(0.5);

    this.moving_speed = +properties.moving_speed;
    this.moving_distance = +properties.moving_distance;
    this.vertical = properties.vertical; 
    this.statix = properties.statix;
    if(this.statix=="true")
        this.frame = 1;
    // saving previous x to keep track of walked distance
    this.previous_x = this.x;
    this.previous_y = this.y;

    this.game_state.game.physics.arcade.enable(this);
    this.body.allowGravity = false; 
    this.body.immovable = true;  // Super importante, sin esto, el objeto se mueve apenas entra en contacto con any shit


    this.vertical = properties.vertical; // movimiento vertical
    this.falls = properties.falls; // despues de un tiempo lo jode la gravedad

    this.realvelocity = properties.direction * this.moving_speed;

    if(this.vertical=="true")
        this.body.velocity.y = this.realvelocity;
    else 
        this.body.velocity.x = this.realvelocity;


};

Platformer.DynamicBlocks.prototype = Object.create(Platformer.Prefab.prototype);
Platformer.DynamicBlocks.prototype.constructor = Platformer.DynamicBlocks;

Platformer.DynamicBlocks.prototype.update = function () {
    "use strict";
    this.game_state.game.physics.arcade.collide(this, this.game_state.layers.collision);
    this.game_state.game.physics.arcade.collide(this, this.game_state.groups.timecoins);

    if(this.vertical!="true"){
        // change the direction if walked the maximum distance
        if (Math.abs(this.x - this.previous_x) >= this.moving_distance) {
            this.body.velocity.x *= -1;
            this.realvelocity = this.body.velocity.x;
            this.previous_x = this.x;
        }
    }else{
        // change the direction if walked the maximum distance
        if (Math.abs(this.y - this.previous_y) >= this.moving_distance) {
            this.body.velocity.y *= -1;
            this.realvelocity = this.body.velocity.y;
            this.previous_y = this.y;
        }
    }
};

