var Platformer = Platformer || {};

Platformer.Planet = function (game_state, position, properties) {
    "use strict";
    Platformer.DynamicBlocks.call(this, game_state, position, properties);
    this.body.allowGravity = false; 
    this.isPlanet = true; 
    this.diagonal = properties.diagonal;
    this.radius = properties.radius;
    if(this.radius>0)
        this.circular = true; 
    else
        this.circular = false;

    if(this.vertical=="true" || this.diagonal=="true" && !this.circular)
        this.body.velocity.y = this.realvelocity;
    if(this.vertical!="true" || this.diagonal=="true" && !this.circular)
        this.body.velocity.x = this.realvelocity;



};

Platformer.Planet.prototype = Object.create(Platformer.DynamicBlocks.prototype);
Platformer.Planet.prototype.constructor = Platformer.Planet;

Platformer.Planet.prototype.update = function () {
    "use strict";
    this.game_state.game.physics.arcade.collide(this, this.game_state.layers.collision);

    if(this.vertical!="true" || this.diagonal=="true" && !this.circular){
        // change the direction if walked the maximum distance
        if (Math.abs(this.x - this.previous_x) >= this.moving_distance) { 
            this.body.velocity.x *= -1;
            this.realvelocity = this.body.velocity.x;
            this.previous_x = this.x;
        }
    }
    if(this.vertical=="true" || this.diagonal=="true" && !this.circular) {
        // change the direction if walked the maximum distance
        if (Math.abs(this.y - this.previous_y) >= this.moving_distance) {
            this.body.velocity.y *= -1;
            this.realvelocity = this.body.velocity.y;
            this.previous_y = this.y;
        }
    }
    if(this.circular) {
        var period = game.time.now * 0.001;
        this.x = this.previous_x + Math.cos(period) * this.radius;
        this.y = this.previous_y + Math.sin(period) * this.radius;
    }



};