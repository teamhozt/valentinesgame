var Platformer = Platformer || {};
var MaxAcceleration = 150;

Platformer.FlyingPlayer = function (game_state, position, properties) {
    "use strict";
    Platformer.Player.call(this, game_state, position, properties);
    
    // Leyes Especiales
    //this.walking_speed = +properties.walking_speed;
    ////this.floating_speed = +properties.floating_speed;
    //this.jumping_speed = +properties.jumping_speed;

    //Limitando la velocidad del jugador
   	this.body.drag.set(100);
    this.body.maxVelocity.set(200);

    // flying player is not affected by gravity
    this.body.allowGravity = false;
    
    this.isFlying = true; 

};

Platformer.FlyingPlayer.prototype = Object.create(Platformer.Player.prototype);
Platformer.FlyingPlayer.prototype.constructor = Platformer.FlyingPlayer;