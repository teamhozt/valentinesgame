var Platformer = Platformer || {};

Platformer.HoztCoins = function (game_state, position, properties) {
    "use strict";
    Platformer.Prefab.call(this, game_state, position, properties);
    this.anchor.setTo(0.5);
    this.animations.add("rotatecoin", [0, 1, 2], 5, true);
    this.animations.play("rotatecoin");
    this.game_state.game.physics.arcade.enable(this);
    this.body.allowGravity = false;
    this.super = properties.super; // con esto sabemos si es de las monedas super o que es la verga

};

Platformer.HoztCoins.prototype = Object.create(Platformer.Prefab.prototype);
Platformer.HoztCoins.prototype.constructor = Platformer.HoztCoins;

Platformer.HoztCoins.prototype.update = function () {
    "use strict";
    this.game_state.game.physics.arcade.collide(this, this.game_state.layers.collision); 
};