var Platformer = Platformer || {};

Platformer.Goal = function (game_state, position, properties) {
    "use strict";
    Platformer.Prefab.call(this, game_state, position, properties);
    
    this.next_level = properties.next_level;
    
    this.game_state.game.physics.arcade.enable(this);
    // saving previous x to keep track of walked distance
    this.previous_x = this.x;
    this.previous_y = this.y;
    
    this.anchor.setTo(0.5);

    this.animations.add("moving", [0,1], 5, true);
    this.animations.play("moving");
    this.blackhole = properties.blackhole;

    if(this.blackhole=="true"){
        this.body.allowGravity = false;
    }

    
};

Platformer.Goal.prototype = Object.create(Platformer.Prefab.prototype);
Platformer.Goal.prototype.constructor = Platformer.Goal;

Platformer.Goal.prototype.update = function () {
    "use strict";
    this.game_state.game.physics.arcade.collide(this, this.game_state.layers.collision);
   
    this.game_state.game.physics.arcade.overlap(this, this.game_state.groups.players, this.reach_goal, null, this);

    if(this.blackhole=="true"){
       var period = game.time.now * 0.001;
        this.x = this.previous_x + Math.sin(period) * 8;
        this.y = this.previous_y + Math.cos(period) * 8; 
    }


};

Platformer.Goal.prototype.reach_goal = function () {
    "use strict";
    walked = false;
    youshallpass = true;
    this.game_state.dialog(lvlname,'end');
    totalscore = totalscore + scorecoins + (timeleft*50); 
    scorebylvl[lvlname]= scorecoins;
    timeleftbylvl[lvlname]= timeleft;
    scorecoins = 0;
    //levelCounter++;
    //console.log(timeleftbylvl);
    //console.log(scorebylvl); 
    
};

