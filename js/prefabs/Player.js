var Platformer = Platformer || {};
var walked = false;
var scorecoins = 0;
var totalscore = 0; 
var hammertime; //stop, hammer time
var lives = 5; //HEY

Platformer.Player = function (game_state, position, properties) {
    "use strict";
    Platformer.Prefab.call(this, game_state, position, properties);
    
    // Leyes standar
    this.walking_speed = +properties.walking_speed;
    this.jumping_speed = +properties.jumping_speed;
    this.bouncing = +properties.bouncing;
    
    this.game_state.game.physics.arcade.enable(this);
    this.body.collideWorldBounds = false;

    hammertime = this.game.time.events.add(180000, this.out_of_time,this);

    //El primer parametro es el tiempo en milisegundos, el segundo parametro es la funcion a ejecutar al finalizar el tiempo
    //y el tercer parametro es el context
    this.animations.add("walking", [0,1,2,1], 6, true);
    this.animations.add("jump", [2,2], 6, true);
    
    this.frame = 3;
    
    this.anchor.setTo(0.5);
    
    this.cursors = this.game_state.game.input.keyboard.createCursorKeys();
        

};

Platformer.Player.prototype = Object.create(Platformer.Prefab.prototype);
Platformer.Player.prototype.constructor = Platformer.Player;

Platformer.Player.prototype.update = function () {
    "use strict";
    
    //Collisions, its all about collisions, like a big fucking bang
    this.game_state.game.physics.arcade.collide(this, this.game_state.layers.collision);
    this.game_state.game.physics.arcade.collide(this, this.game_state.groups.enemies, this.hit_enemy, null, this);
    this.game_state.game.physics.arcade.collide(this, this.game_state.groups.hoztcoins, this.hit_coin, null, this);
    this.game_state.game.physics.arcade.collide(this, this.game_state.groups.timecoins, this.get_time, null, this);
    this.game_state.game.physics.arcade.collide(this, this.game_state.groups.planets, this.bigbang, null, this);
    var isdynamic = this.game_state.game.physics.arcade.collide(this, this.game_state.groups.dynamicblocks, this.dynamicstuff, null, this);
   
    if(this.isFlying){ //si es flying 
        // Arriba y abajo this.rotation = -1.5;
       //Move right
        if ((this.cursors.right.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.D)))
            this.rotation = 0;
        //Move left
        if ((this.cursors.left.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.A))) 
            this.rotation = 3.14159;
        //Move Up
        if ((this.cursors.up.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.W))) 
            this.rotation = 4.71239;
        //Move down
        if ((this.cursors.down.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.S))) 
            this.rotation = 1.5708;
        //Combos
        // right-up
        if ((this.cursors.right.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.D)) && (this.cursors.up.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.W)))
            this.rotation = 5.49779;
        // right-down
        if ((this.cursors.right.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.D)) && (this.cursors.down.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.S)))
            this.rotation = 0.785398;
        // left-up
        if ((this.cursors.left.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.A)) && (this.cursors.up.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.W)))
            this.rotation = 3.92699;
        // left-down
        if ((this.cursors.left.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.A)) && (this.cursors.down.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.S)))
            this.rotation = 2.35619;
        //Check if it's time to stop
        if(this.cursors.left.isUp && this.cursors.right.isUp && this.cursors.up.isUp && this.cursors.down.isUp && !this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.A) && !this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.S) && !this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.D) && !this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.W)){
            this.body.acceleration.set(0);
            this.animations.stop();
            this.frame = 3;
        }else {
            // slow down
            game.physics.arcade.accelerationFromRotation(this.rotation, 200, this.body.acceleration);
            this.animations.play("walking");
            this.scale.setTo(-1, 1);
        } 
    }else{
                 if ((this.cursors.right.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.D) ) && (this.body.velocity.x >= 0 || isNaN(this.body.velocity.x) ) ) {
                // move right
                this.body.velocity.x = this.walking_speed;
                this.scale.setTo(-1, 1);
                walked = true; // si tecleas, guardamos que ya camino

                if (this.body.velocity.y<20) // Solo activamos la animacion si no esta saltando
                    this.animations.play("walking");
                else 
                    this.animations.play("jump");

            } else if ((this.cursors.left.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.A)) && (this.body.velocity.x <= 0 || isNaN(this.body.velocity.x))) {
                // move left
                this.body.velocity.x = -this.walking_speed;
                this.scale.setTo(1, 1);
                walked = true; // si tecleas, guardamos que ya camino

                if (this.body.velocity.y<20) // Solo activamos la animacion si no esta saltando
                    this.animations.play("walking");
                else 
                    this.animations.play("jump");
                
            } else if (this.cursors.up.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.W) || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) ) {
                // move up
                this.animations.stop();
                this.animations.play("jump");
            } else {
                // stop
                this.body.velocity.x = 0;
                this.animations.stop();
                if(walked)
                    this.frame = 4;
                else 
                    this.frame = 3;
            }

    } // si no es flying
   
    //Una pequeña modificacion del salto para incluir los dynamicblocks y poder bajar de ellos
    if (!isdynamic) {
        if (player.body.blocked.down && (this.cursors.up.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.W) || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))) {
            player.body.velocity.y = -this.jumping_speed;
        }
    }
    
    // dies if touches the end of the screen
    if (this.bottom >= (this.game_state.game.world.height+70)) {
        walked = false;
        this.dying(1);
    }

    //WorldBounds a mano, pues quiero que el personaje tenga limite inferior
    if( (this.y <= 30) &&  this.cursors.down.isUp && !this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.S) ) {
        player.body.velocity.y = 100;
    }
    if( (this.x <= 0) &&  this.cursors.right.isUp && !this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.D) ) {
        player.body.velocity.x = 0;
    }
    if( (this.x >= this.game_state.game.world.width) &&  this.cursors.left.isUp && !this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.A) ) {
        player.body.velocity.x = 0;
    }

};
// Funcion que se ejecuta al finalizar el tiempo
Platformer.Player.prototype.out_of_time = function() {
    walked = false;
    this.dying(1);
};

Platformer.Player.prototype.hit_enemy = function (player, enemy) {
    "use strict";
    // if the player is above the enemy, the enemy is killed, otherwise the player dies
    if (enemy.body.touching.up && (enemy.fire!="true")) {
        enemy.kill();
        scorecoins=scorecoins+300;
        player.body.velocity.y = -player.bouncing;
        //console.log("ENEMY KILLED"); 
    } else {
        walked = false;
        this.dying(1);
    }
};

Platformer.Player.prototype.bigbang = function (player, enemy) {
    "use strict";
     var t = setInterval(function(){
        enemy.body.immovable = false;
        clearInterval(t);
    },enemy.falls);
};


Platformer.Player.prototype.dying = function (deads) {
    "use strict";
     lives = lives - deads;
    if(lives>0)//debe ser cero pero para el test estoy cansado del game over
        this.game_state.restart_level();
    else { 
        this.game_state.die_gameover();
    } 
};

Platformer.Player.prototype.hit_coin = function (player, coin) {
    "use strict";
    //console.log("new coin");
    if(gameOptions.playSound)
     game.sound.play('coin-sound');
    coin.kill();
    var valor = (coin.super!="true")  ? 100 : 800;  //si lo se, parece redundante, pero el true lo agarra como string
    scorecoins=scorecoins+valor;
};

Platformer.Player.prototype.get_time = function (player, timecoin) {
    "use strict";
    bonus = timecoin.customtime;
    timecoin.kill();
    var valor = bonus + timeleft;
    game.time.events.remove(hammertime); 
    hammertime = this.game.time.events.add((valor*1000), this.out_of_time,this); 
};

Platformer.Player.prototype.dynamicstuff = function (player, block) {
    var self = this;
    block.body.checkCollision.up = true; //reiniciamos porsiacaso
    if (player.body.touching.down && (self.cursors.up.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.W) || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))) {
        player.body.velocity.y = -self.jumping_speed;
    }
    if (player.body.touching.down && (self.cursors.down.isDown || this.game_state.game.input.keyboard.isDown(Phaser.Keyboard.S)) && block.statix!="true") {
        block.body.checkCollision.up = false; //cuando estas en dynamics blocks, puedes ir para abajo 
    }
    if(block.statix!="true") {
        block.frame =  2;
    }
    if (player.body.touching.down && block.falls>0) { // Para los que se caen al tocarlo el player
         
    var t = setInterval(function(){
        block.body.immovable = false;
        clearInterval(t);
    },block.falls);


    }
};