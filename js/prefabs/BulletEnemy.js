var Platformer = Platformer || {};

Platformer.BulletEnemy = function (game_state, position, properties) {
    "use strict";
    Platformer.Enemy.call(this, game_state, position, properties);
    
    // Con esto el ignora las colisiones del entorno en X
    this.body.customSeparateX = true;

    if(properties.antigravity=="true")
    	this.body.allowGravity = false;
    
    this.animations.add("bullet", [0, 1], 5, true);
    this.animations.play("bullet");
};

Platformer.BulletEnemy.prototype = Object.create(Platformer.Enemy.prototype);
Platformer.BulletEnemy.prototype.constructor = Platformer.BulletEnemy;