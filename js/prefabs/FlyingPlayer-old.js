var Platformer = Platformer || {};
var MaxAcceleration = 150;

Platformer.FlyingPlayer = function (game_state, position, properties) {
    "use strict";
    Platformer.Player.call(this, game_state, position, properties);
    
    // Leyes Especiales
    //this.walking_speed = +properties.walking_speed;
    ////this.floating_speed = +properties.floating_speed;
    //this.jumping_speed = +properties.jumping_speed;

    //Limitando la velocidad del jugador
   	this.body.drag.set(100);
    this.body.maxVelocity.set(200);

    
    this.game_state.game.physics.arcade.enable(this);
    this.body.collideWorldBounds = true;
    
    this.animations.add("walking", [0, 1, 2, 1], 6, true);
    
    this.frame = 3;
    
    this.anchor.setTo(0.5);
    
    this.cursors = this.game_state.game.input.keyboard.createCursorKeys();

    // flying player is not affected by gravity
    this.body.allowGravity = false;
    

};

Platformer.FlyingPlayer.prototype = Object.create(Platformer.Player.prototype);
Platformer.FlyingPlayer.prototype.constructor = Platformer.FlyingPlayer;

Platformer.FlyingPlayer.prototype.update = function () {
    "use strict";
    this.game_state.game.physics.arcade.collide(this, this.game_state.layers.collision);
    //this.game_state.game.physics.arcade.collide(this, this.game_state.groups.enemies, player.hit_enemy, null, this); // PARA PROBAR PORQUE EL HIT ESTA EN PLAYER NO EN FLYING PLAYER
   

   // Arriba y abajo this.rotation = -1.5;
   //Move right
    if (this.cursors.right.isDown)
  		this.rotation = 0;
    //Move left
    if (this.cursors.left.isDown) 
  		this.rotation = 3.14159;
    //Move Up
    if (this.cursors.up.isDown) 
        this.rotation = 4.71239;

    //Move down
    if (this.cursors.down.isDown) 
        this.rotation = 1.5708;

    //Combos
    // right-up
    if (this.cursors.right.isDown && this.cursors.up.isDown)
        this.rotation = 5.49779;
    // right-down
    if (this.cursors.right.isDown && this.cursors.down.isDown)
        this.rotation = 0.785398;
    // left-up
    if (this.cursors.left.isDown && this.cursors.up.isDown)
        this.rotation = 3.92699;
    // left-down
    if (this.cursors.left.isDown && this.cursors.down.isDown)
        this.rotation = 2.35619;

    //Check if it's time to stop
    if(this.cursors.left.isUp && this.cursors.right.isUp && this.cursors.up.isUp && this.cursors.down.isUp){
          
        this.body.acceleration.set(0);
        this.animations.stop();
        this.frame = 3;

    }else {
        
        // slow down
        game.physics.arcade.accelerationFromRotation(this.rotation, 200, this.body.acceleration);
        this.animations.play("walking");
        this.scale.setTo(-1, 1);

    }


    /*if (this.cursors.up.isDown) {
        // move down
        this.body.y--;
        this.animations.play("walking");
        this.scale.setTo(-1, 1);
    }if (this.cursors.down.isDown) {
        // move up
        this.body.y++;
        this.animations.play("walking");
        this.scale.setTo(1, 1);
    }
    */
	    // dies if touches the end of the screen
    if (this.bottom >= this.game_state.game.world.height) {
        this.game_state.restart_level();
    }
};
