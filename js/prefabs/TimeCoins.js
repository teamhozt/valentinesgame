var Platformer = Platformer || {};

Platformer.TimeCoins = function (game_state, position, properties) {
    "use strict";
    Platformer.Prefab.call(this, game_state, position, properties);
    this.anchor.setTo(0.5);
    this.animations.add("rotatecoin", [0, 1, 2, 3], 5, true);
    this.animations.play("rotatecoin");
    this.game_state.game.physics.arcade.enable(this);
    this.body.allowGravity = false; 
    this.customtime = parseInt(properties.customtime);


};

Platformer.TimeCoins.prototype = Object.create(Platformer.Prefab.prototype);
Platformer.TimeCoins.prototype.constructor = Platformer.TimeCoins;

Platformer.TimeCoins.prototype.update = function () {
    "use strict";
    this.game_state.game.physics.arcade.collide(this, this.game_state.layers.collision); 
};