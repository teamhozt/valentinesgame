var Phaser = Phaser || {};
var Platformer = Platformer || {};
var Credits = function(){};


var ActualGroup;

var ilustradores = Array();
	ilustradores[0] = {name:"Mariangel Briceño", picture:"mariangel", instagram:"@mariangel1620"};	
	ilustradores[1] = {name:"Nubia Navarro", picture:"nubia", instagram:"@nubianavarro"};	
	ilustradores[2] = {name:"Génesis Quintero", picture:"gene", instagram:"@chamitachama_"};	
	
var programadores = Array();
	programadores[0] = {name:"Darwin Jimenez", picture:"darwin", instagram:"@d4rw9"};	
	programadores[1] = {name:"Tomas Landaeta", picture:"tomas", instagram:"@tomas_landaeta"};	
	programadores[2] = {name:"Eduardo Franco ", picture:"edo", instagram:"@edofranco_"};	

var sponsors = Array();
	sponsors[0] = {name:"JOKUKUMA", picture:"yoku", instagram:"@jokukumave"};	
	sponsors[1] = {name:"Forever Frida", picture:"frida", instagram:"@laforeverfrida"};	
	sponsors[2] = {name:"MoffCafé", picture:"moff", instagram:"@moffcafe"};

var sponsors2 = Array();
	sponsors2[0] = {name:"Posdata Papeleria", picture:"posdata", instagram:"@posdatapapeleria"};	
	sponsors2[1] = {name:"Sorte", picture:"sorte", instagram:"@__sorte"};	
	sponsors2[2] = {name:"MAR casa de diseño", picture:"mar", instagram:"@marcasadediseno"};	
	

var titles = Array();
	titles[0]  = "Ilustradores Invitados: ";
	titles[1]  = "Desarrolladores:";
	titles[2]  = "Sponsors (premios):";
	titles[3]  = "Desarrollado por HOZT ";



var textStyle = 	{font: '14pt JoyStix',align: 'center',fill: '#232323'};

Credits.prototype = {

	init: function(){

		//Transicion
		this.game.stateTransition.configure({
		  duration: Phaser.Timer.SECOND * 0.5,
		  ease: Phaser.Easing.Exponential.InOut,
		  properties: {
		    alpha: 0,
		    scale: {
		      x: 1.4,
		      y: 1.4
		    }
		  }
		});


		
		

		this.Title = game.make.text(700,180, "Estas son las personas que ayudaron en este proyecto", textStyle);


	},
	preload: function(){
		//Loading image
		
		game.load.image('credits-bg','assets/images/credits.png');

		game.load.image('byhozt', 'assets/images/byhozt.png');
		// setting style
		this.back = game.make.text(0,window.innerHeight, "Back",textStyle);


		//Activando listeners
		this.back.inputEnabled = true;

		// onclick
		this.back.events.onInputUp.add(function(){
			console.log("Let's do this!");
			setTimeout(function(){
				this.game.stateTransition.to('GameMenu');
			},1000);

		});

		// hover

		this.back.events.onInputOver.add(function (target){
			target.setStyle(htextStyle);
		});

		this.back.events.onInputOut.add(function (target){
			target.setStyle(textStyle);
		});


	},
	create: function(){

		// Fondo
				// Se agrega un fondo
		var bg = game.add.sprite(0,0,'credits-bg');
		bg.width = window.innerWidth;
		bg.height = window.innerHeight;

		game.stage.disableVisibilityChange = true;
		
		game.add.existing(this.Title).anchor.setTo(0.5);


		var ref = this.Title;
		var c = 0;
		var t = setInterval(function(){
			
			if(ActualGroup != undefined)
				killemall(ActualGroup);

		 	switch(c){
		 		case 0:
		 			ref.setText('Desarrolladores:');
		 			ActualGroup = createList(programadores);
		 		break;
		 		case 1:
		 			ref.setText('Ilustradores Invitados:');
		 			ActualGroup = createList(ilustradores);

		 		break;
		 		case 2:
		 			ref.setText('Sponsors (premios):');
		 			ActualGroup = createList(sponsors);
		 		break;
		 		case 3:
		 			ref.setText('Sponsors (premios):');
		 			ActualGroup = createList(sponsors2);
		 		break;
		 		case 4:
		 			killemall(ActualGroup);
		 			ref.kill();
		 			/*
		 			var hozt = game.add.sprite(game.world.centerX,game.world.centerY+50,'hozt');
		 			game.add.existing(hozt).anchor.setTo(0.5);
		 			hozt.scale.setTo(1.5,1.5);
		 			*/
		 			// Se cambia el fondo // fix logo
					bg = game.add.sprite(0,0,'byhozt');
					bg.scale.setTo(0.2, 0.2);


					bg.x = window.innerWidth  * 0.4;
					bg.y = window.innerHeight * 0.4;

		 				

		 			clearInterval(t);

					setTimeout(function(){
						this.game.stateTransition.to('GameMenu');
					},1000);	 			
		 		break;
		 	}

		 	c++;
		},1500);

		function createList(group){
		
			var innerGroup = game.add.group();

			var x = window.innerWidth * 0.25;
			var y = window.innerHeight * 0.55;

			for (var i = 0; i < group.length; i++) {

					var ref = createDisplay(x,y,group[i]);
					game.add.existing(ref).anchor.setTo(0.5);
					innerGroup.add(ref);
					x += 350;

				};	
			
			return innerGroup;

		}

		function createDisplay(x,y,profile){

			// Se crea la imagen
			var bmd =  game.make.bitmapData(150, 250);
			bmd.alphaMask(profile.picture, 'mask');
			var display = game.make.image(x,y,bmd);

			// Se crea el texto
			var name = game.make.text(0,0,profile.name,textStyle)
			name.anchor.setTo(0.5);

			name.y = display.y * 0.15;

			var insta =  game.make.text(0,0,profile.instagram, textStyle)
			insta.y = name.x;
			insta.y = name.y + name.height;
			insta.anchor.setTo(0.5);

			display.addChild(name);
			display.addChild(insta);
			// on click
				name.inputEnabled = true;	
				insta.inputEnabled = true;	
				display.inputEnabled = true;
			var toInsta = function(){
				var url = 'http://www.instagram.com/'+profile.instagram.substring(1, profile.instagram.length);
				var win = window.open(url, '_blank');
					if(win){
					    //Browser has allowed it to be opened
					    win.focus();
					}else{
					    //Broswer has blocked it
					    alert('Please allow popups for this game');
					}
			};

		name.events.onInputUp.add(toInsta);
		insta.events.onInputUp.add(toInsta);
		display.events.onInputUp.add(toInsta);
		
		return display;
		}

		function killemall(group){
			for (var i = 0; i < group.length; i++) {
				group.getAt(i).kill();
			}
		}


	},
	update: function(){
		

	}

};