var Phaser = Phaser || {};
var Platformer = Platformer || {};

var nlvl, lvlname; 
var timeleft = 0;
var bonus = 0;
var enterdialog=0; 
var buttonSound;
var buttonMusic;
var onSound = 1;
var offSound = 0;
var onMusic = 1;
var offMusic = 0;
var finaldialog = false; 
var marditasea = false; //ESTO ES PARA DEBUGGEAR
var front_layer;
var selfy; 
var youshallpass = false; 
var t; 

Platformer.TiledState = function () {
    "use strict";
    Phaser.State.call(this);
};

Platformer.TiledState.prototype = Object.create(Phaser.State.prototype);
Platformer.TiledState.prototype.constructor = Platformer.TiledState;

Platformer.TiledState.prototype.init = function (level_data) {
    "use strict";
    this.level_data = level_data;
    
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    this.scale.pageAlignHorizontally = true;
    this.scale.pageAlignVertically = true;
    
    // start physics system
    this.game.physics.startSystem(Phaser.Physics.ARCADE);
    this.game.physics.arcade.gravity.y = 1000;
    
    // create map and set tileset
    this.map = this.game.add.tilemap(level_data.map.key);
    this.map.addTilesetImage(this.map.tilesets[0].name, level_data.map.tileset);

    this.lvlname = level_data.map.lvlname;
    this.nlvl = level_data.map.nlvl;
    nlvl = this.nlvl;
    lvlname = this.lvlname;


};
Platformer.TiledState.prototype.preload = function() {
   // game.load.spritesheet('button',  'assets/images/spritesmusic.png', 19, 20);
};

Platformer.TiledState.prototype.create = function () {
    "use strict";
    var group_name, object_layer, collision_tiles; 
    var bg = game.add.sprite(0,0, 'bg_lvl'); // Con esto cambia el fondo dependiendo del json 
    buttonSound = game.add.button(160, 15, 'button', this.soundOfforOn , this, offSound, onSound, offSound, onSound);
    buttonSound.fixedToCamera = true;
    buttonMusic = game.add.button(200, 15, 'button', this.musicOfforOn, this, 2, 3, 2, 3);
    buttonMusic.fixedToCamera = true;
    selfy = this;
    youshallpass = false; 

    if(lvlname=="Maracaibo") {
        totalscore = 0; 
        scorebylvl = [];
        timeleftbylvl = [];
    }

       
    // create map layers
    this.layers = {};
    this.map.layers.forEach(function (layer) {
        this.layers[layer.name] = this.map.createLayer(layer.name);
        if (layer.properties.collision) { // collision layer
            collision_tiles = [];
            layer.data.forEach(function (data_row) { // find tiles used in the layer
                data_row.forEach(function (tile) {
                    // check if it's a valid tile index and isn't already in the list
                    if (tile.index > 0 && collision_tiles.indexOf(tile.index) === -1) {
                        collision_tiles.push(tile.index);
                    }
                }, this);
            }, this);
            this.map.setCollision(collision_tiles, true, layer.name);
        }
    }, this);
    // resize the world to be the size of the current layer
    this.layers[this.map.layer.name].resizeWorld();
    
    // create groups
    this.groups = {};
    this.level_data.groups.forEach(function (group_name) {
        this.groups[group_name] = this.game.add.group();
    }, this);
    
    this.prefabs = {};
    
    for (object_layer in this.map.objects) {
        if (this.map.objects.hasOwnProperty(object_layer)) {
            // create layer objects
            this.map.objects[object_layer].forEach(this.create_object, this);
        }
    }

    enterdialog++;

//Calling start Dialog
    if(enterdialog==1 && !marditasea)
        this.dialog(lvlname,'start');

   //Dios del tiempo 
   timeleft=180;
   game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer);
   scorecoins = 0; // reiniciamos las monedas del nivel


   //VAMO A CALMARNO (PAUSA)
    front_layer = game.add.group();
    var whiteStyle =  {font: '18pt JoyStix',align: 'center',fill: 'white'};
    var thePause = game.make.text(window.innerWidth * 0.5, window.innerHeight * 0.5, '',whiteStyle); 
    thePause.anchor.setTo(0.5);
    front_layer.add(thePause);
    front_layer.fixedToCamera = true;

    var pauseKEY = game.input.keyboard.addKey(Phaser.Keyboard.P);
    pauseKEY.onDown.add(function(){
        if(!youshallpass){
            this.game.paused = !this.game.paused;
            if(this.game.paused)
                thePause.setText('PAUSE');
            else 
                thePause.setText('');
        }
    }, this);

    var spaceKEY = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    var calmate = true; 

    spaceKEY.onDown.add(function(){

        if(youshallpass && calmate)
        {
            clearInterval(t);
            this.nexy_lvl(); 
            youshallpass=false; 
            setTimeout(function(){  calmate= true; }, 1000);

        }
        //console.log(youshallpass);

       
    }, this);



    
};

Platformer.TiledState.prototype.updateTimer = function() {
    if(!this.game.paused)
        timeleft = timeleft - 1 + bonus; 
    bonus = 0;        
};

Platformer.TiledState.prototype.render = function() {
  this.game.debug.text("TIEMPO: " + timeleft, 20, 30, "white", "14px JoyStix"); // Time
  this.game.debug.text("PUNTAJE: " + (totalscore + scorecoins), 20, 50, "white", "14px JoyStix"); // Score
  //this.game.debug.text(this.nlvl + " " + this.lvlname, 20, 50, "#00ff00", "10px Courier"); // Lvl name
  this.game.debug.text("VIDAS: " + lives, 20, 70, "white", "14px JoyStix"); // Score  


  
};
var player; 
Platformer.TiledState.prototype.create_object = function (object) {
    "use strict";
    var position, prefab;
    // tiled coordinates starts in the bottom left corner
    position = {"x": object.x + (this.map.tileHeight / 2), "y": object.y - (this.map.tileHeight / 2)};
    // create object according to its type
    switch (object.type) {
    case "player":
        if(this.lvlname=="Space")
            player = new Platformer.FlyingPlayer(this, position, object.properties);
        else
            player = new Platformer.Player(this, position, object.properties);
        //the camera will follow the player in the world
        this.game.camera.follow(player);
        break;
    case "dynamicblocks":
    prefab = new Platformer.DynamicBlocks(this, position, object.properties);
    break;
    case "ground_enemy":
        prefab = new Platformer.Enemy(this, position, object.properties);
        break;
    case "flying_enemy":
        prefab = new Platformer.FlyingEnemy(this, position, object.properties);
        break;
    case "bullet_enemy":
        prefab = new Platformer.BulletEnemy(this, position, object.properties);
        break;
    case "hoztcoins":
        prefab = new Platformer.HoztCoins(this, position, object.properties);
        break;
    case "timecoins":
        prefab = new Platformer.TimeCoins(this, position, object.properties);
        break;
    case "planets":
        prefab = new Platformer.Planet(this, position, object.properties);
        break;
    case "goal":
        prefab = new Platformer.Goal(this, position, object.properties);
        break;
    }
    this.prefabs[object.name] = prefab;
};

Platformer.TiledState.prototype.restart_level = function () {
    "use strict";
    enterdialog=-1;
    this.game.state.restart(true, false, this.level_data);
};


Platformer.TiledState.prototype.die_gameover = function () {
    "use strict";
    lives = 5; 
    totalscore = totalscore + scorecoins + (timeleft*10); 
    scorebylvl[lvlname]= scorecoins;
    timeleftbylvl[lvlname]= timeleft;
    this.game.stateTransition.to('GameOver');
};


Platformer.TiledState.prototype.update = function () {

    if(this.game.input.keyboard.isDown(Phaser.Keyboard.L) && marditasea)
        this.nexy_lvl();

}

Platformer.TiledState.prototype.nexy_lvl = function () {
        levelCounter++;
        if(lvlname=="Space")
            this.game.stateTransition.to('EndState');
        else
            this.game.stateTransition.to('LevelSplash'); 
}




    /*
        DIALOG BOX FAST
    */

Platformer.TiledState.prototype.dialog = function(city,state){  

    var dialog;
    var text;
    var profile;
        
    var json = game.cache.getJSON(city);
    var conversation = json[state];
    var index = 0;
    var lines = 0;
    var timer = 0;


    this.game.paused = true;
    var onetime = true; 
    t = setInterval(function(){
        if(state=="end")
         youshallpass = true; 

        if(index<(conversation.length+1)){
            if(index<(conversation.length)){
                if(lines<conversation[index]['lines'].length){
                    setProfile(conversation[index]['character']);
                    setText(conversation[index]['lines'][lines]);
                    lines++;
                }else{
                    lines = 0;
                    index++;
                    if(state!="end")
                        index++;
                }           
            }else{
                if(state=="end"){
                    setProfile('Cupido'+nlvl);
                    setText('Tu puntaje en este nivel fue: ' + scorebylvl[lvlname] + ' Bonificación de tiempo: '+ (timeleftbylvl[lvlname]*10));
                    if(onetime){
                        setTimeout(function(){ 
                            index++; 
                            finaldialog = true;
                            setText('Presiona espacio para continuar');  //FALTA ARMAR ESTO MEJOR
                            youshallpass = true; 
                        }, 3000);
                        onetime = false; 
                    }
                }else
                    index++; 
            }  
            
        }else if(state!="end"){
            clearInterval(t);
            killit();
            this.game.paused = false;
            return true; 
        }

    },1800);

    /*
        Funciones internas para el dialogo
    */
    function createDialog(){
            dialog = game.add.sprite(game.camera.x + window.innerWidth * 0.1, 600 ,'dialog');
            dialog.width = window.innerWidth * 0.8;
            dialog.height = window.innerHeight * 0.3;
            dialog.fixedToCamera = true;    
        }

    
    function killit(){
        dialog.kill();
        text.kill();
        profile.kill();
    }
    function setProfile(character){
        if(profile == null){
            createProfile(character);
        }else{

            if(profile.character != character){
                profile.kill();
                createProfile(character);
            }
            
        }
  
    }
    function createProfile(character){
        if(dialog === undefined)
            createDialog();

        profile = game.add.sprite(dialog.x + 6,dialog.y + 6,character);
        //profile.width = dialog.width * 0.205;
        profile.height = dialog.height * 0.94;
        profile.width = profile.height;
        profile.fixedToCamera = true;
        profile.character = character;
    }

    function setText(line){
        if(text === undefined){
            var textStyle = { font: "16pt JoyStix", fill: "white", wordWrap: true, wordWrapWidth: dialog.width * 0.7, align: "left"};
            text = game.add.text(dialog.x  + profile.width * 1.2, dialog.y + 50, line, textStyle);
            text.fixedToCamera = true;
        }else
            text.setText(line);
    }

};

    /*
        FIN DE DIALOG BOX FAST
    */

Platformer.TiledState.prototype.soundOfforOn = function() {
    gameOptions.playSound = !gameOptions.playSound;
    if(onSound == 1){
        onSound = 0;
        offSound = 1;
        buttonSound.setFrames(offSound, onSound, offSound, onSound);
    }else{
        onSound = 1;
        offSound = 0;
        buttonSound.setFrames(offSound, onSound, offSound, onSound);
    }
    
            
};

Platformer.TiledState.prototype.musicOfforOn = function() {
    gameOptions.playMusic = !gameOptions.playMusic;

    if(gameOptions.playMusic){
                musicPlayer.volume  = 0.3;
                musicPlayer.play();
            }else{
                musicPlayer.pause();
            }  
    if(onMusic == 1){
        onMusic = 0;
        offMusic = 1;
        buttonMusic.setFrames(3, 2, 3, 2);
    }else{
        onMusic = 1;
        offMusic = 0;
        buttonMusic.setFrames(2, 3, 2, 3);
    }

};