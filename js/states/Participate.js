var Phaser = Phaser || {};
var Platformer = Platformer || {};
var Participate = function(){};

var bgtile;
var groupy;
var titleGlobal;
var subtitleGlobal;
var emailInput;
var buttonGlobal;
var buttonGlobal2;
var mplayer = {};
var num = true;

Participate.prototype = {

	init: function(){
	},
	preload: function(){
		
		game.load.image('gameover-bg','assets/images/bg_lvl1.jpg');
		

		this.title = game.make.text(logox ,logoy - 50 ,'REGISTRA TU PUNTAJE',{
			font: '20pt JoyStix',
			fill: '#232323',
			align: 'center'

		});

		this.subtitle1 = game.make.text(logox,logoy - 20,'Anunciaremos los ganadores el 01/03/2016 por las redes sociales de HOZT',{
			font: '12pt JoyStix',
			fill: '#232323',
			align: 'center'

		});
		

		this.subtitle2 = game.make.text(logox,logoy + 220,'',{
			font: '14pt sans',
			fill: '#232323',
			align: 'center'

		});

		buttonGlobal = game.make.text(logox,logoy + 130,'Enviar',{
													font: '14pt JoyStix',
													fill: '#232323',
													align: 'center'
												});

		buttonGlobal2 = game.make.text(logox, 420,'Registrar',{
											font: '14pt JoyStix',
											fill: '#232323',
											align: 'center'

										});




	},

	create: function(){
		// Fondo con ciclo
		//bgtile = game.add.tileSprite(0, 0, window.innerWidth, game.cache.getImage('gameover-bg').height, 'gameover-bg');



		// Se agrega un fondo
		var bg = game.add.sprite(0,0,'credits-bg');
		bg.width = window.innerWidth;
		bg.height = window.innerHeight;

		this.logo = game.make.sprite(logox, (logoy-150),  'lilbrand');
		

		game.add.existing(this.logo).fixedToCamera = true;
		game.add.existing(this.logo).anchor.setTo(0.5);
		

		game.add.existing(this.title).fixedToCamera = true;
		game.add.existing(this.title).anchor.setTo(0.5);

		game.add.existing(this.subtitle1).fixedToCamera = true;
		game.add.existing(this.subtitle1).anchor.setTo(0.5);

		game.add.existing(this.subtitle2).fixedToCamera = true;
		game.add.existing(this.subtitle2).anchor.setTo(0.5);
		
		groupy = game.add.group();
		groupy.add(this.subtitle1) 
		groupy.add(this.subtitle2);
		
		titleGlobal = this.title;
		subtitleGlobal = this.subtitle1;



		this.myInput = createInput(1000, logoy + 50 ,window.innerWidth * 0.30, 380,'Email');
		emailInput = this.myInput;
		

		game.add.existing(buttonGlobal).inputEnabled = true;
		//game.add.existing(buttonGlobal).fixedToCamera = true;
		game.add.existing(buttonGlobal).x = centerSpriteX(buttonGlobal.width);
		game.add.existing(buttonGlobal).bringToTop();

		var getText = function (){
    		console.log(emailInput.canvasInput.value());
    		var email = emailInput.canvasInput.value();
    		
    		if(validateEmail(email))
    			checkPlayer(email,buttonGlobal,buttonGlobal2);
    		else
    			emailInput.canvasInput.value('Correo invalido!');
    	}

    	game.add.existing(buttonGlobal).inputEnabled = true;
		game.add.existing(buttonGlobal).events.onInputUp.add(getText);
		



	    function checkPlayer(mEmail,button,button2){
	
		$.ajax({
			 url: 'php/scores.php',
			 type: 'POST', 
			 data: {operation: "check",email: mEmail,scores: { score : totalscore, level : nlvl, scorelvl1 : scorebylvl['Maracaibo'], scorelvl2 : scorebylvl['LosAndes'] ,scorelvl3 : scorebylvl['Mochima'], scorelvl4 : scorebylvl['Caracas'], scorelvl5 : scorebylvl['Space'], timeleft1 : timeleftbylvl['Maracaibo'], timeleft2 : timeleftbylvl['LosAndes'] ,timeleft3 : timeleftbylvl['Mochima'], timeleft4 : timeleftbylvl['Caracas'], timeleft5 : timeleftbylvl['Space'] }}
			})
			.done(function(msj){
				console.log('Data recived');
				var object = jQuery.parseJSON(msj);
				if(object.status == 'OK'){
					titleGlobal.setText('Hola ' + object.name + '');
					subtitleGlobal.setText("ya registramos tu nuevo puntaje");
					emailInput.kill();

					button.setText('Volver');
					button.events.onInputUp.add(function(){
						game.state.start('GameMenu');
					});

					
				}else{
					titleGlobal.kill();
					button.kill();
					killemall(groupy);

					mplayer.email = emailInput.canvasInput.value();
					emailInput.kill();

					var name = createInput(0, 200,window.innerWidth * 0.3, 380,'NOMBRE');
					var telefono = createInput(0, 240,window.innerWidth * 0.3, 380,'TELÉFONO');
					var twitter = createInput(0, 280,window.innerWidth * 0.3, 380,'TWITTER');
					var instagram = createInput(0, 320,window.innerWidth * 0.3, 380,'INSTAGRAM');
					var facebook = createInput(0, 360,window.innerWidth * 0.3, 380,'FACEBOOK');
					
					game.add.existing(button2).inputEnabled = true;
					//game.add.existing(buttonGlobal).fixedToCamera = true;
					game.add.existing(button2).x = centerSpriteX(buttonGlobal.width);
					game.add.existing(button2).bringToTop();
			    	game.add.existing(button2).inputEnabled = true;

		


					var getData = function (){
						
						mplayer.name = name.canvasInput.value();
						mplayer.cellphone = telefono.canvasInput.value();
						mplayer.twitter = twitter.canvasInput.value();
						mplayer.instagram = instagram.canvasInput.value();
						mplayer.facebook = facebook.canvasInput.value();
						if(num){
							Login();
							num = false;
						}


					}

					game.add.existing(button2).events.onInputUp.add(getData);


				}
			})
			.fail(function(){
				console.log("Something happen while making the requst");
			})
			.always(function(){
				console.log("Ajax completed");
			});
 		}

 		function Login(){
 			console.log(scorebylvl);
 			console.log(scorebylvl['Maracaibo']);
 			console.log(timeleftbylvl['Maracaibo']);
 			$.ajax({
			 url: 'php/scores.php',
			 type: 'POST', 
			 data: {operation: "post",player: mplayer,scores: { score : totalscore, level : nlvl, scorelvl1 : scorebylvl['Maracaibo'], scorelvl2 : scorebylvl['Los Andes'] ,scorelvl3 : scorebylvl['Mochima'], scorelvl4 : scorebylvl['Caracas'], scorelvl5 : scorebylvl['Space'], timeleft1 : timeleftbylvl['Maracaibo'], timeleft2 : timeleftbylvl['Los Andes'] ,timeleft3 : timeleftbylvl['Mochima'], timeleft4 : timeleftbylvl['Caracas'], timeleft5 : timeleftbylvl['Space'] }}
			})
			.done(function(msj){
				console.log('Data recived');
				console.log(msj);

				var object = jQuery.parseJSON(msj);
				if(object.status == 'OK'){
					
					alert('Listo, ya registramos tu nuevo puntaje');
					alert("Gracias por participar");
						
					setTimeout(function(){game.state.start('GameMenu')},800);
						
				}else{

					setTimeout(function(){game.state.start('GameMenu')},800);
				}

			})
			.fail(function(){
				console.log("Something happen while making the requst");
			})
			.always(function(){
				console.log("Ajax completed");
			});
 		}

 		function validateEmail(email) {
		    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		    return re.test(email);
		}

		function killemall(){
			for (var i = 0; i < groupy.length; i++) {
				groupy.getAt(i).kill();
			}
		}

		function createInput(x, y, Width,Height,holder){
		  	console.log('hola');
		    var bmd = game.add.bitmapData(Width, Height);
		    x = centerSpriteX(Width);
		    var myInput = this.game.add.sprite(x, y, bmd);
		    
		    myInput.canvasInput = new CanvasInput({
		      canvas: bmd.canvas,
		      fontSize: 15,
		      fontFamily: 'JoyStix',
		      fontColor: '#212121',
		      fontWeight: 'bold',
		      width: Width,
		      padding: 8,
		      borderWidth: 1,
		      borderColor: '#000',
		      borderRadius: 3,
		      boxShadow: '1px 1px 0px #fff',
		      innerShadow: '0px 0px 5px rgba(0, 0, 0, 0.5)',
		      placeHolder: holder
		    });

		    myInput.inputEnabled = true;
		    myInput.events.onInputUp.add(function(){
		    	myInput.canvasInput.focus();

		    });
		    
		    return myInput;
  		}
			 function centerSpriteX(width){
				var tamano = window.innerWidth;
				var restante = tamano - width;
				var result = 0;
				if(restante>0){
					result = (restante/2);
				}
				return result;
			}

	},
	update: function(){
		//bgtile.tilePosition.x -= 1;
		//buttonGlobal.x  -= 1;

	}
};

