var Phaser = Phaser || {};
var Platformer = Platformer || {};
var GameOver = function(){};

GameOver.prototype = {

	init: function(){
	},
	preload: function(){
		
		game.load.image('gameover-bg','assets/images/bg_lvl'+nlvl+'.jpg');

		// setting style
		var hoverStyle =	{	
							font: 'bold 16pt JoyStix',
							align: 'center',
							strokeThickness: 0,
							fill: '#F05F68',
							stroke: 'black'
							};

		var overstyle = 	{
							font: 'bold 16pt JoyStix',
							fill: '#232323',
							align: 'center',	
							strokeThickness: 0,
							stroke: 'black'
							};


		this.gameoverText = game.make.text(window.innerWidth * 0.25,100, "Game Over", {
			font: 'bold 60pt JoyStix',
			fill: '#232323',
			align: 'center',
			strokeThickness: 0,
			stroke: 'black'

		});

		posY = this.gameoverText.y + this.gameoverText.height + 60;

		this.scoreText = game.make.text(window.innerWidth * 0.5,posY, "Puntaje: " + totalscore, {
							font: 'bold 30pt JoyStix',
							fill: '#232323',
							align: 'center',	
							strokeThickness: 0,
							stroke: 'black'
							});
	
		posY = this.scoreText.y + 70;

		this.tryText = game.make.text(window.innerWidth * 0.5,posY, "Volver a jugar", overstyle);

		posY = this.tryText.y + 35;
		
		this.share = game.make.text(window.innerWidth * 0.5,posY, "Compartir", overstyle);
		
		posY = this.share.y + 35;
		
		this.participate = game.make.text(window.innerWidth * 0.5,posY, "Registra tu puntaje",overstyle); 
		

		posY = this.participate.y + 35;
		
		this.toMenu = game.make.text(window.innerWidth * 0.5,posY, "Menú Principal",overstyle);


		//Activando listeners
		this.tryText.inputEnabled = true;	
		this.participate.inputEnabled = true;	
		this.share.inputEnabled = true;
		this.toMenu.inputEnabled = true;

		// onclick
		this.tryText.events.onInputUp.add(function(){
			console.log("Let's do this!");
                        levelCounter = 0;
			setTimeout(function(){
				
				this.game.stateTransition.to('LevelSplash',true,false);
			},1000);

		});

		this.participate.events.onInputUp.add(function(){
			console.log("I want to win!");
                        levelCounter = 0; // ESTE MALDITO
			this.game.stateTransition.to('Participate',true,false);
		});

		this.share.events.onInputUp.add(function() {
  			window.open('https://www.facebook.com/sharer/sharer.php?u=http://desflechados.com.ve','1455458326347','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=0,left=0,top=0');
  			return false;
		});


		this.toMenu.events.onInputUp.add(function() {
			levelCounter = 0; // ESTE MALDITO
			this.game.stateTransition.to('GameMenu',true,false);
		});


		// hover

		this.participate.events.onInputOver.add(function (target){
			target.setStyle(hoverStyle);
		});

		this.participate.events.onInputOut.add(function (target){
			target.setStyle(overstyle);
		});

		this.share.events.onInputOver.add(function (target){
			target.setStyle(hoverStyle);
		});

		this.share.events.onInputOut.add(function (target){
			target.setStyle(overstyle);
		});


		this.tryText.events.onInputOver.add(function (target){
			target.setStyle(hoverStyle);
		});

		this.tryText.events.onInputOut.add(function (target){
			target.setStyle(overstyle);
		});


		this.toMenu.events.onInputOver.add(function (target){
			target.setStyle(hoverStyle);
		});

		this.toMenu.events.onInputOut.add(function (target){
			target.setStyle(overstyle);
		});
		//Transicion


				this.game.stateTransition.configure({
				  duration: Phaser.Timer.SECOND * 0.8,
				  ease: Phaser.Easing.Exponential.InOut,
				  properties: {
				    alpha: 0,
				    scale: {
				      x: 1.4,
				      y: 1.4
				    }
				  }
				});


	},
	create: function(){
		// Fondo con ciclo

		var elyideal = window.innerHeight - 876; 

		bgtile = game.add.tileSprite(0, elyideal, window.innerWidth, game.cache.getImage('gameover-bg').height, 'gameover-bg');

		game.stage.disableVisibilityChange = true;
		game.stage.backgroundColor = 0x0000;
		var width = game.add.existing(this.gameoverText).width;

		game.add.existing(this.gameoverText).x = centerSpriteX(width);

		game.add.existing(this.gameoverText).fixedToCamera = true;

		game.add.existing(this.scoreText).anchor.setTo(0.5);
		game.add.existing(this.tryText).anchor.setTo(0.5);

	
		game.add.existing(this.share).anchor.setTo(0.5);
		game.add.existing(this.participate).anchor.setTo(0.5);

		game.add.existing(this.toMenu).anchor.setTo(0.5);

		///game.add.existing(this.gameoverText).x = centerSpriteX(game.add.existing(this.gameoverText).width);

		 function centerSpriteX(width){
				var tamano = window.innerWidth;
				var restante = tamano - width;
				var result = 0;
				if(restante>0){
					result = (restante/2);
				}
				return result;
		}
	},
	update: function(){
		bgtile.tilePosition.x -= 1;

	}
};