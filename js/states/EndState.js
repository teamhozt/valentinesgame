var Phaser = Phaser || {};
var Platformer = Platformer || {};
var EndState = function(){};

var bg;

EndState.prototype = {

	init: function(){
	},
	preload: function(){
		game.load.image('TheEnd','assets/images/endsprite.png');
		
		

		// setting style
		var hoverStyle =	{	
							font: 'bold 25pt JoyStix',
							align: 'center',
							strokeThickness: 10,
							fill: 'red',
							stroke: 'black'
							};

		var overstyle = 	{
							font: 'bold 25pt JoyStix',
							fill: 'white',
							align: 'center',	
							strokeThickness: 10,
							stroke: 'black'
							};




		//Transicion


				this.game.stateTransition.configure({
				  duration: Phaser.Timer.SECOND * 0.8,
				  ease: Phaser.Easing.Exponential.InOut,
				  properties: {
				    alpha: 0,
				    scale: {
				      x: 1.4,
				      y: 1.4
				    }
				  }
				});
	},
	create: function(){
		// Fondo con ciclo

		var bg = game.add.sprite(0,0,'TheEnd');
		bg.width = window.innerWidth;
		bg.height = window.innerHeight;
	
		game.stage.disableVisibilityChange = true;
		game.stage.backgroundColor = 0x0000;

		this.dialog('Cuarto','final');

	},
	update: function(){

	},

	dialog: function(city,state){  

    var dialog;
    var text;
    var profile;
        
    var json = game.cache.getJSON(city);
    var conversation = json[state];
    var index = 0;
    var lines = 0;
    var timer = 0;
    
    var t = setInterval(function(){
         
        if(index<(conversation.length+1)){
            if(index<(conversation.length)){
                if(lines<conversation[index]['lines'].length){
                    setProfile(conversation[index]['character']);
                    setText(conversation[index]['lines'][lines]);
                    lines++;
                }else{
                    lines = 0;
                    index++;
                    if(state!="end")
                        index++;
                }           
            }else{
                if(state=="end"){
                    setProfile('Cupido');
                    setText('Tu puntaje en este nivel fue: ' + scorebylvl[lvlname] + ' Bonificación de tiempo: '+ (timeleftbylvl[lvlname]*10));

                    setTimeout(function(){ index++; }, 3000);
                }else
                    index++; 
            }  
            
        }else{
            clearInterval(t);
            killit();
            this.game.paused = false;
            this.game.stateTransition.to('GameOver');
        }

    },1800);

    /*
        Funciones internas para el dialogo
    */
    function createDialog(){
            dialog = game.add.sprite(window.innerWidth * 0.15,window.innerHeight * 0.02,'dialog');
            dialog.width = window.innerWidth * 0.7;
            dialog.height = window.innerHeight * 0.3;
            dialog.fixedToCamera = true;    
        }

    
    function killit(){
        dialog.kill();
        text.kill();
        profile.kill();
    }
    function setProfile(character){
        if(profile == null){
            createProfile(character);
        }else{

            if(profile.character != character){
                profile.kill();
                createProfile(character);
            }
            
        }
  
    }
    function createProfile(character){
        if(dialog === undefined)
            createDialog();

        profile = game.add.sprite(dialog.x + 6,dialog.y + 6,character);
        //profile.width = dialog.width * 0.205;
        profile.height = dialog.height * 0.94;
        profile.width = profile.height;
        profile.fixedToCamera = true;
        profile.character = character;
    }

    function setText(line){
        if(text === undefined){
            var textStyle = { font: "16pt JoyStix", fill: "white", wordWrap: true, wordWrapWidth: dialog.width * 0.7, align: "left"};
            text = game.add.text(dialog.x  + profile.width * 1.2, dialog.y + 50, line, textStyle);
            text.fixedToCamera = true;
        }else
            text.setText(line);
    }

	}
};