var Phaser = Phaser || {};
var Platformer = Platformer || {};
var RankingState = function(){};


var ActualGroup;
var cloud_layer;
var text_layer;

var rankStyle = 	{font: '28pt JoyStix',align: 'center',fill: 'white'};
var scoreStyle = 	{font: '16pt JoyStix',align: 'center',fill: '#232323'};
var h;
var w;
var aux;
var self; 
var ix;
var idx;
var veces; 
var onebyone2;
var telotengo = false; 
var object; 

RankingState.prototype = {

	init: function(){

		//Transicion
		this.game.stateTransition.configure({
		  duration: Phaser.Timer.SECOND * 0.5,
		  ease: Phaser.Easing.Exponential.InOut,
		  properties: {
		    alpha: 0,
		    scale: {
		      x: 1.4,
		      y: 1.4
		    }
		  }
		});			

		// Fondo
		game.stage.backgroundColor = 0x39BCA3;
		//Nubes
		h = window.innerHeight;
		w = window.innerWidth;



	},
	preload: function(){


		game.load.image('brand','assets/images/logo.png');
		game.load.image('nube-1','assets/images/nube-1.png');
		game.load.image('nube-2','assets/images/nube-2.png');
		game.load.image('byhozt2','assets/images/byhozt2.png');


	},
	create: function(){
onebyone2 = false;
ix = 3;
idx = 0;
veces = 0; 
		// Key 
		/* ESTO SERVIRA PARA LA PAUSA DEL DIALOGO 
		aux = true;
		 keySPACE = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
   		 keySPACE.onDown.add(function(){
   		 		if(aux)
				this.game.stateTransition.to('GameMenu');
				aux =false;
   		 }, this);
		*/

		//Loading image
		cloud_layer = game.add.group();
		text_layer = game.add.group();

		// Creando nubes 

		this.nube1 = cloud_layer.create(w*0.2,h*0.1,'nube-1');
		this.nube2 = cloud_layer.create(w*0.7,h*0.2,'nube-2');

		//Clouds Scales
		this.nube1.scale.setTo(0.25,0.25);
		this.nube2.scale.setTo(0.2,0.2);


		// Creando el logo
		this.logo = text_layer.create(w*0.38,h*0.09,'brand');
		this.logo.width = w*0.23;
		this.logo.height = h*0.13;

		// Creando otro logo
		this.logo = text_layer.create(w*0.79,h*0.93,'byhozt2');
		
		this.logo.inputEnabled = true;	
		var toHozt = function(){		
							window.open("www.hozt.com.ve",'1455458326347','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=0,left=0,top=0');					
						};
		this.logo.events.onInputUp.add(toHozt);

		//this.logo.width = w*0.20;
		//this.logo.height = h*0.05;


		this.Title = game.make.text(w*0.5,h*0.25, "Mejores Puntajes", rankStyle);
		this.Title.anchor.setTo(0.5);
		text_layer.add(this.Title);

		nameX = w*0.35;
		scoreX = w*0.60;

		startY = h*0.40;

		game.stage.disableVisibilityChange = true;


			for (var i = 0; i < 20; i++) {
				startY = h * (0.35 + (.05 * i));
				var name = game.make.text(nameX,startY,'',scoreStyle);
				var score = game.make.text(scoreX,startY,'',scoreStyle);
				text_layer.add(name);
				text_layer.add(score);
				

			}

		self = this; 

		//game.add.existing(this.logo).anchor.setTo(0.5);

		if(!telotengo)
			getScores();
		else 
			self.setList();


		
	
		function getScores(){	

			$.ajax({
			 url: 'php/scores.php',
			 type: 'GET'
			})
			.done(function(msj){
				console.log('Data recived');


				object = jQuery.parseJSON(msj);
				console.log(object);
				telotengo=true;
				self.setList();


			})
			.fail(function(){
				console.log("Something happen while making the requst");
			})
			.always(function(){
				console.log("Ajax completed");
			});
		}




	},
	update: function(){
		this.nube1.x += 1.1;
		this.nube2.x += 1.1

		this.screenWrap(this.nube1);
		this.screenWrap(this.nube2);

		if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && onebyone2){
			if(veces!=20) {
				self.clearList();
				self.setList();
			}else
				self.game.stateTransition.to('GameMenu');
		 	onebyone2=false;
		 	setTimeout(function(){ onebyone2=true; }, 1000); 
		}


	},
	setList: function(){
			scores = object; 
			i = ix; 
			var cleanclean; 
			var t = setInterval(function(){

						//name
						if(scores[idx].name.length  > 16)
							scores[idx].name = scores[idx].name.substring(0,16);

						var elementName = text_layer.getAt(i);
						elementName.setText(scores[idx].name);

						//Trampa
						elementName.idx = idx;

						// onclick
						if(scores[idx].instagram != undefined){
							elementName.inputEnabled = true;
							scores[idx].instagram = scores[idx].instagram.replace("@","");
							scores[idx].instagram = "http://www.instagram.com/"+scores[idx].instagram;

							var toInsta = function(target){	
								var pos = target.idx;
								window.open(scores[pos].instagram,'1455458326347','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=0,left=0,top=0');					
									};

							elementName.events.onInputUp.add(toInsta);
							console.log(scores[idx].instagram);
						}else
							elementName.events.removeAll();
						


						//score
						text_layer.getAt(i + 1).setText(scores[idx].s);
						i = i+2;
						idx++;

						
						veces++;
						if(veces==10 || veces==20){
							clearInterval(t);
							console.log('stop');
							var exittext = game.make.text(w * 0.5, h * 0.9, 'ESPACIO PARA CONTINUAR',whiteStyle);
							exittext.anchor.setTo(0.5);
							text_layer.add(exittext);
							onebyone2 = true;
							ix=3;
						}

					},400);
			
	},
	clearList: function(){
		for (var i = 3; i < text_layer.length; i++) 
				text_layer.getAt(i).setText(' ');
	},
	screenWrap: function(sprite) {

    if (sprite.x < 0)
    {
        sprite.x = game.width;
    }
    else if (sprite.x > game.width)
    {
        sprite.x = 0;
    }

    if (sprite.y < 0)
    {
        sprite.y = game.height;
    }
    else if (sprite.y > game.height)
    {
        sprite.y = 0;
    }

}

};