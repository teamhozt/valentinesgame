var Splash = function () {};
var Phaser = Phaser || {};
var Platformer = Platformer || {};

Splash.prototype = {

	loadScripts: function(){
		
		//External libs
		game.load.script('WebFont','js/vendor/webfontloader/webfontloader.js');
		game.load.script('CanvasInput','js/vendor/CanvasInput/CanvasInput.js');
		game.load.script('jQuery','js/vendor/jQuery/jquery-1.12.0.min.js');

		// Libs
		game.load.script('style','js/libs/style.js');
		game.load.script('mixins','js/libs/mixins.js');



		//Game states
		game.load.script('Gamemenu', 'js/states/GameMenu.js?fuckcache='+timexstamp);
		game.load.script('Game','js/states/Game.js?fuckcache='+timexstamp);
		game.load.script('GameOver', 'js/states/GameOver.js?fuckcache='+timexstamp);
		game.load.script('Credits', 'js/states/Credits.js?fuckcache='+timexstamp);
		game.load.script('Options', 'js/states/Options.js?fuckcache='+timexstamp);
		game.load.script('Participate', 'js/states/Participate.js?fuckcache='+timexstamp);
		game.load.script('RankingState', 'js/states/RankingState.js?fuckcache='+timexstamp);
		game.load.script('EndState', 'js/states/EndState.js?fuckcache='+timexstamp);
		game.load.script('SponsorState', 'js/states/SponsorState.js?fuckcache='+timexstamp);
		//Loading edo scripts
		game.load.script("BootState", "js/states/BootState.js");
		game.load.script("LoadingState", "js/states/LoadingState.js");
		game.load.script("GameState", "js/states/TiledState.js?fuckcache="+timexstamp);

		// Cargando edo scripts (si otra vez, se me olvido esto)
	   game.load.script("Prefab","js/prefabs/Prefab.js");
       game.load.script("Player","js/prefabs/Player.js?fuckcache="+timexstamp);;
       game.load.script("FlyingPlayer","js/prefabs/FlyingPlayer.js");
       game.load.script("DynamicBlocks","js/prefabs/DynamicBlocks.js");
       game.load.script("Enemy","js/prefabs/Enemy.js");
       game.load.script("FlyingEnemy","js/prefabs/FlyingEnemy.js");
       game.load.script("BulletEnemy","js/prefabs/BulletEnemy.js");
       game.load.script("HoztCoins","js/prefabs/HoztCoins.js");
       game.load.script("TimeCoins","js/prefabs/TimeCoins.js");
       game.load.script("Planet","js/prefabs/Planet.js"); 
       game.load.script("Goal","js/prefabs/Goal.js?fuckcache="+timexstamp);


       //Level Splash
       game.load.script('LevelSplash', 'js/states/LevelSplash.js');
       //Dialogos
       game.load.json('Maracaibo','assets/dialogs/Maracaibo.json');
       game.load.json('Mochima','assets/dialogs/Mochima.json');
       game.load.json('LosAndes','assets/dialogs/LosAndes.json');
       game.load.json('Caracas','assets/dialogs/Caracas.json');
       game.load.json('Space','assets/dialogs/Space.json');
       game.load.json('Cuarto','assets/dialogs/Cuarto.json');

	},


	loadBgm: function(){
		//Background Music
		game.load.audio('intro', 'assets/bgm/cassius.wav');
		//game.load.audio('overworld', 'assets/bgm/Birdy.mp3');
		game.load.audio('hover-menuitem','assets/bgm/NFF-good-tip-low.wav');
		game.load.audio('click-menuitem','assets/bgm/NFF-good-tip-high.wav');
		game.load.audio('coin-sound','assets/bgm/hozt-coin.wav');
	},

	loadImages: function(){
		game.load.image('menu-bg','assets/images/sky.png');
		game.load.image('options-bg','assets/images/room.gif');
		game.load.image('gameover-bg','assets/images/bg_lvl1.jpg');
		game.load.image('credits-bg','assets/images/credits.png');

		game.load.image('dialog','assets/images/dialog.png');
		//TEST PARA EL DIALOG RUBI
		game.load.image('Rubi1', 'assets/images/avatar_rubi1.png');
		game.load.image('Rubi2', 'assets/images/avatar_rubi2.png');
		game.load.image('Rubi3', 'assets/images/avatar_rubi3.png');
		game.load.image('Rubi4', 'assets/images/avatar_rubi4.png');
		game.load.image('Rubi5', 'assets/images/avatar_rubi5.png');
		game.load.image('Rubi6', 'assets/images/avatar_rubi6.png');
		game.load.image('Chuo', 'assets/images/avatar_chuo.png');
		game.load.image('Cheo', 'assets/images/avatar_cheo.png');
		game.load.image('Daniel', 'assets/images/avatar_daniel.png');
		game.load.image('Camila', 'assets/images/avatar_camila.png');
		game.load.image('Cupido', 'assets/images/avatar_cupido.png');
		game.load.image('Cupido1', 'assets/images/avatar_cupido1.png');
		game.load.image('Cupido2', 'assets/images/avatar_cupido2.png');
		game.load.image('Cupido3', 'assets/images/avatar_cupido3.png');
		game.load.image('Cupido4', 'assets/images/avatar_cupido4.png');
		game.load.image('Cupido5', 'assets/images/avatar_cupido5.png');
		//TEST CREDITS
		game.load.image('byhozt', 'assets/images/byhozt.png');
		// Ilustradoras
		game.load.image('gen', 'assets/images/gen.png');
		game.load.image('mariangel', 'assets/images/mariangel.png');
		game.load.image('nubia', 'assets/images/nubia-foto.png');
		
		//Desarrolladores
		game.load.image('dar', 'assets/images/dar.png');
		game.load.image('edo', 'assets/images/edo.png');
		game.load.image('tomas', 'assets/images/tomas.png');

		game.load.image('facebook','assets/images/hover-1.png');
		game.load.image('twitter','assets/images/hover-2.png');
		game.load.image('instagram','assets/images/hover-3.png');

		// Elementos extras
		game.load.image('sp_cupid1','assets/images/sp_cupid1.png');
		game.load.image('sp_cupid2','assets/images/sp_cupid2.png');
		game.load.image('sp_cupid3','assets/images/sp_cupid3.png');
		game.load.image('sp_cupid4','assets/images/sp_cupid4.png');
		game.load.image('sp_cupid5','assets/images/sp_cupid5.png');
		game.load.image('sp_ovni','assets/images/sp_ovni.png');
		game.load.image('sp_cheo','assets/images/sp_cheo.png');
		game.load.image('sp_chuo','assets/images/sp_chuo.png');
		game.load.image('sp_camila','assets/images/sp_camila.png');
		game.load.image('sp_daniel','assets/images/sp_daniel.png');
		game.load.image('sp_rubi','assets/images/sp_rubi.png');
 		
 		//Logos
 		game.load.image('hozt', 'assets/images/logob.png');
		game.load.image('hozt-credits', 'assets/images/hozt-credits.png');

		game.load.spritesheet('button',  'assets/images/spritesmusic.png', 19, 20);
		//CONTROLS
		//game.load.image('music', 'assets/images/spritesmusic.png');
			


	},

	loadFonts: function(){
		WebFontConfig = {
			custom: {
				families: ['JoyStix'],
				urls: ['assets/style/joystixmonospace.css']
			}
		}
	},

	// the preload function then will call all of the previously defined functions:
	preload: function(){
		//Init transitions

		
		this.game.stateTransition = this.game.plugins.add(Phaser.Plugin.StateTransition);
		this.game.stateTransition.configure({
				  duration: Phaser.Timer.SECOND * 0.8,
				  ease: Phaser.Easing.Exponential.InOut,
				  properties: {
				    alpha: 0,
				    scale: {
				      x: 1.4,
				      y: 1.4
				    }
				  }
				});

		var bg = game.add.sprite(0,0, 'bkground');
		bg.width = window.innerWidth;bg.height = window.innerHeight;

		this.logo = game.make.sprite(game.world.centerX, game.world.centerY-75,  'brand');
		
		game.add.existing(this.logo).anchor.setTo(0.5)
		
		var posy = this.logo.height + this.logo.y + 45;

		this.Title = game.make.text(game.world.centerX,posy, "Cargando",{
            font: 'bold 20pt JoyStix',
            fill: 'white',
            align: 'left'

        });
		game.add.existing(this.Title).anchor.setTo(0.5)


		//this.loadingBar = game.make.sprite(game.world.centerX, 400, "loading");

		//game.add.existing(this.loadingBar).anchor.set(0.5);

		this.load.setPreloadSprite(this.logo);

		this.loadScripts();
		this.loadFonts();
		this.loadImages();

		this.loadBgm();

	},

	addGameStates: function(){
		game.state.add("GameMenu",GameMenu);
		game.state.add("Game",Game);
		game.state.add("GameOver",GameOver);
		game.state.add("Credits",Credits);
		game.state.add("Options",Options);
		game.state.add("Participate",Participate);
		game.state.add("RankingState",RankingState);
		game.state.add("EndState",EndState);
		game.state.add("SponsorState",SponsorState);

		
		//Loading edo States
		game.state.add("BootState", new Platformer.BootState());
		game.state.add("LoadingState", new Platformer.LoadingState());
		game.state.add("GameState", new Platformer.TiledState());


		// New Splash
		game.state.add("LevelSplash",LevelSplash);
	},
	addGameMusic: function(){
		musicPlayer = game.add.audio('intro');
		musicPlayer.loop = true
		if(gameOptions.playMusic)
		musicPlayer.play();
	},
	create: function(){

		this.addGameStates();
		this.addGameMusic();
		this.Title.setText('¡Listo!');
		setTimeout(function(){
			logox = game.world.centerX;
			logoy = game.world.centerY-75;
			game.state.start('GameMenu');
		},1000);
	}
}