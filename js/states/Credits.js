var Phaser = Phaser || {};
var Platformer = Platformer || {};
var Credits = function(){};


var ActualGroup;

var collabs = Array();
	collabs[0] = {name:"Génesis Quintero", picture:"gen", instagram:"https://www.instagram.com/chamitachama_", facebook : "https://www.facebook.com/genquinteroo", group: "ilustradores"};
	collabs[1] = {name:"Nubia Navarro", picture:"nubia", instagram:"https://www.instagram.com/nubianavarro", facebook: "https://www.facebook.com/profile.php?id=708568073", twitter: "https://www.twitter.com/nu_bikini",group: "ilustradores"};	
	collabs[2] = {name:"Mariangel Briceño", picture:"mariangel", instagram:"https://www.instagram.com/mariangel1620",  facebook:"https://www.facebook.com/mariangel1620", group: "ilustradores"};	
	collabs[3] = {name:"Darwin Jimenez", picture:"dar", instagram:"https://www.instagram.com/d4rw9", twitter:"https://www.twitter.com/Darwin_JC", group: "desarrolladores"};	
	collabs[4] = {name:"Tomas Landaeta", picture:"tomas", instagram:"https://www.instagram.com/tomas_landaeta", group: "desarrolladores"};	
	collabs[5] = {name:"Eduardo Franco ", picture:"edo", instagram:"https://www.instagram.com/edofranco_", twitter:"https://www.twitter.com/edofranco_", facebook : "https://www.facebook.com/egofranco", group: "desarrolladores"};	


var titles = Array();
	titles[0]  = "Ilustradores Invitados: ";
	titles[1]  = "Desarrolladores:";
	titles[2]  = "Sponsors (premios):";
	titles[3]  = "Desarrollado por HOZT ";



var colabStyle = 	{font: '20pt JoyStix',align: 'center',fill: 'white'};
var bg;
var collab;

var credits_front_layer,cp_layer;
var h,w;
var ref;
var onebyone; 
Credits.prototype = {

	init: function(){
		
		//Transicion
		this.game.stateTransition.configure({
		  duration: Phaser.Timer.SECOND * 0.5,
		  ease: Phaser.Easing.Exponential.InOut,
		  properties: {
		    alpha: 0,
		    scale: {
		      x: 1.4,
		      y: 1.4
		    }
		  }
		});


		
		h = window.innerHeight;
		w = window.innerWidth;
		max_alto =  h*0.68;
		max_ancho = w*0.25;
	},
	preload: function(){
		//Loading image
		game.load.image('space','assets/images/bg_lvl5.jpg');


	},
	create: function(){
		onebyone = true; 

		// Fondo
		bg = game.add.tileSprite(0, 0, window.innerWidth, game.cache.getImage('space').height, 'space');
		game.stage.disableVisibilityChange = true;

		
		// layer front 
		credits_front_layer = game.add.group();
		sp_layer = game.add.group();

		this.Title = game.make.text(w*0.68,w*0.10, "ILUSTRADORES", colabStyle);
		game.add.existing(this.Title).anchor.setTo(0.5);

		// Profile picture		
		collab = credits_front_layer.create(w*0.58,h*0.25,'gen');
		collab.width = w*0.20;
		collab.height = collab.width;


		//redes
		facebook = credits_front_layer.create(w*0.65,h*0.74,'facebook');
		twitter = credits_front_layer.create(w*0.675,h*0.74,'twitter');
		instagram = credits_front_layer.create(w*0.7,h*0.74,'instagram');
		this.setClicks(collabs[0]);
		
		// name 
		this.name = game.make.text(this.Title.x,h*0.7, "Genesis Quintero", colabStyle);
 		game.add.existing(this.name).anchor.setTo(0.5);

		credits_front_layer.add(this.name);
		

		 ref = this.name;

		// Big sprite
		bigSprite = sp_layer.create(w*0.15,h*0.17,'sp_cupid2');
		bigSprite.height = max_alto;
		bigSprite.width = max_alto;
		

		

		function randOrd(){
		  return (Math.round(Math.random())-0.5);
		}

			
		var exittext = game.make.text(window.innerWidth * 0.5, h * 0.90, 'ESPACIO PARA CONTINUAR',whiteStyle);
		credits_front_layer.add(exittext);
		exittext.anchor.setTo(0.5);


	},
	setClicks: function(collab){
			console.log("Hola");
			facebook.inputEnabled = true;	
			twitter.inputEnabled = true;	
			instagram.inputEnabled = true;

				if(collab.instagram != undefined){
					var toInsta = function(){			
						window.open(collab.instagram,'1455458326347','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=0,left=0,top=0');					
					};

					instagram.events.onInputUp.add(toInsta);
				}else
					instagram.events.onInputUp.removeAll();
				
					
				if(collab.facebook != undefined){
					var toFace = function(){			
						window.open(collab.facebook,'1455458326347','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=0,left=0,top=0');					
					};
					facebook.events.onInputUp.add(toFace);

				}else
					facebook.events.onInputUp.removeAll();

				if(collab.twitter != undefined){
					var toTwitter = function(){			
					window.open(collab.twitter,'1455458326347','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=0,left=0,top=0');					
					};
					twitter.events.onInputUp.add(toTwitter);

				}else
					twitter.events.onInputUp.removeAll();
				

			
			
	},
	update: function(){
		
		bg.tilePosition.x -= 1;



		if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && onebyone){
			if(c < collabs.length){

			 	this.Title.setText(collabs[c].group);
			 	this.name.setText(collabs[c].name);

			 	bigSprite.loadTexture(lasimagenes[c]);

		 		collab.loadTexture(collabs[c].picture);

		 		this.setClicks(collabs[c]);

			}
			else{
				c = 0; 
				this.game.stateTransition.to('GameMenu');
			}
		 	c++;
		 	onebyone=false;
		 	setTimeout(function(){ onebyone=true; }, 1000);
		}

	}

};