var Phaser = Phaser || {};
var Platformer = Platformer || {};
var GameMenu = function(){};

GameMenu.prototype = {
		// We'll just let the className fall back to the default setting
	  // so there's no need to put it in our menuConfig...
	menuConfig: {
	    startY: game.world.centerY + 80,
	    startX: 'center'
	},
	init: function(){

	},
	preload: function(){
		this.optionCount = 1;
	},
	create: function(){
		
		game.stage.disableVisibilityChange = true;

		var bg = game.add.sprite(0, 0, 'menu-bg');
		bg.width = window.innerWidth;
		bg.height = window.innerHeight;

		this.logo = game.make.sprite(logox, logoy,  'brand');
		
		game.add.existing(this.logo).anchor.setTo(0.5);

		this.addMenuOption('COMIENZA', function(){
			console.log('You clicked Start!');
			setTimeout(function(){
				this.game.stateTransition.to('BootState', true, false, levels[levelCounter]); //PARA CAMBIAR EL LVL DE INICIO
			},1000);

		},"default");

		this.addMenuOption('OPCIONES', function(){
			game.state.start("Options");
		},"default");

		this.addMenuOption('CRÉDITOS', function(){
			game.state.start('Credits');
		},"default");


		this.addMenuOption('SPONSORS', function(){
			game.state.start('SponsorState');
		},"default");

		this.addMenuOption('MEJORES PUNTAJES', function(){
			game.state.start('RankingState');
		},"default");



	}
};

Phaser.Utils.mixinPrototype(GameMenu.prototype, mixins);
