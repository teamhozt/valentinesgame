var Phaser = Phaser || {};
var Platformer = Platformer || {};
var LevelSplash = function(){};

LevelSplash.prototype = {

	init: function(){


	},
	preload: function(){
		//Transicion
				this.game.stateTransition.configure({
				  duration: Phaser.Timer.SECOND * 0.8,
				  ease: Phaser.Easing.Exponential.InOut,
				  properties: {
				    alpha: 0,
				    scale: {
				      x: 1.4,
				      y: 1.4
				    }
				  }
				});
	},
	create: function(){


		this.levelText = game.make.text(window.innerWidth * 0.5 ,200, "LEVEL " + (levelCounter + 1), {
			font: 'bold 80pt JoyStix',
			fill: 'white',
			align: 'left'

		});

		this.levelText.fixedToCamera = true;
		var posY = this.levelText.height/4 + game.world.centerY;

		this.cityText = game.make.text(window.innerWidth * 0.5,posY, nameOfLevel[levelCounter], {
			font: 'bold 40pt JoyStix',
			fill: 'white',
			align: 'left'

		});
		this.cityText.fixedToCamera = true;
		//this.levelText.setShadow(3, 3, 'black', 5);
		this.optionCount = 1;

		
		game.stage.disableVisibilityChange = true;
		game.stage.backgroundColor = 0x000000;
		game.add.existing(this.levelText).anchor.setTo(0.5);
		game.add.existing(this.cityText).anchor.setTo(0.5);

		//This goes in game menu after clicking start
		setTimeout(function(){
			this.game.stateTransition.to('BootState', true, false, levels[levelCounter]); //PARA CAMBIAR EL LVL DE INICIO

		},1000);
			

	},

	update : function(){
		
	}
};
