var Phaser = Phaser || {};
var Platformer = Platformer || {};
var SponsorState = function(){};
var cloud_layer;
var front_layer;

var ActualGroup;
var bigSprite;
var sponsor;
var facebook;
var twiter;
var instagram;

var max_ancho, max_alto;
var h,w;
var c = 1;
var ref;
var onebyone=true; 
//var lasimagenes = ['sp_cupid1','sp_cupid2','sp_cupid3','sp_cupid4','sp_cupid5','sp_ovni','sp_cheo','sp_chuo','sp_camila','sp_daniel','sp_rubi'];
var lasimagenes = ['sp_cupid1','sp_cupid2','sp_cupid3','sp_cupid4','sp_cupid5','sp_cupid1','sp_cupid2','sp_cupid3','sp_cupid4','sp_cupid5','sp_cupid1','sp_cupid2','sp_cupid3','sp_cupid4','sp_cupid5','sp_cupid1','sp_cupid2','sp_cupid3','sp_cupid4','sp_cupid5'];
//lasimagenes.sort(randOrd);

var sponsors = Array();
	sponsors[0] = {name:"Moff Café", picture:"moff", instagram:"http://www.instagram.com/moffcafe",facebook:"https://www.facebook.com/pages/Moff-Cafe/141298159579064"};
	sponsors[1] = {name:"Forever Frida", picture:"ffrida", instagram:"https://www.instagram.com/laforeverfrida", facebook : "https://www.facebook.com/laforeverfrida/", twitter: "https://twitter.com/laforeverfrida"};	
	sponsors[2] = {name:"Jōkūkuma", picture:"joku", instagram:"https://www.instagram.com/jokukuma/", facebook : "https://www.facebook.com/jokukuma/", twitter: "https://twitter.com/jokukuma"};	
	sponsors[3] = {name:"Posdata Papeleria", picture:"pd", instagram:"https://instagram.com/posdatapapeleria",facebook:"https://www.facebook.com/posdatapapeleria/?fref=ts"};
	sponsors[4] = {name:"Sorte", picture:"sorte", instagram:"http://www.instagram.com/__sorte", facebook:"https://www.facebook.com/sorte.tienda/"};	
	sponsors[5] = {name:"Sargento Pimienta", picture:"sag", instagram:"https://www.instagram.com/elsgtopimienta/",twitter:"https://twitter.com/elsgtopimienta?lang=es", facebook : "https://www.facebook.com/elsgtopimienta/"};
	sponsors[6] = {name:"Band-Aids", picture:"bandaids", instagram:"http://instagram.com/bandaidsradio", facebook:"https://www.facebook.com/Bandaidsradio", twitter:"https://twitter.com/BandAidsRadio"};
	sponsors[7] = {name:"GM design", picture:"gm", facebook: "https://www.facebook.com/GMmasDESIGN",instagram:"http://instagram.com/gmmasdesign", twitter: "https://twitter.com/GMmasDESIGN"};
	sponsors[8] = {name:"MAR Casa de Diseño", picture:"mar", instagram:"https://instagram.com/marcasadediseno/", facebook:"https://www.facebook.com/mar.casadediseno/"};
	sponsors[9] = {name:"Indie Soma", picture:"indie-soma", instagram:"https://instagram.com/indiesoma/", facebook:"https://www.facebook.com/indiesoma/"};
	sponsors[10] = {name:"Salas Animation", picture:"salas", instagram:"https://www.instagram.com/salasanimationstudio/", facebook:"https://www.facebook.com/salasanimationstudio"};
	sponsors[11] = {name:"Sueño con Aruba", picture:"sueno", instagram:"https://www.instagram.com/suenoconaruba/", facebook:"https://www.facebook.com/SuenoConAruba/?ref=ts&fref=ts", twitter:"https://twitter.com/suenoconaruba"};
		//
	



var textStyle = 	{font: '20pt JoyStix',align: 'center',fill: '#232323'};
var whiteStyle = 	{font: '18pt JoyStix',align: 'center',fill: 'white'};
SponsorState.prototype = {

	init: function(){

		//Transicion
		this.game.stateTransition.configure({
		  duration: Phaser.Timer.SECOND * 0.5,
		  ease: Phaser.Easing.Exponential.InOut,
		  properties: {
		    alpha: 0,
		    scale: {
		      x: 1.4,
		      y: 1.4
		    }
		  }
		});

		// Fondo
		game.stage.backgroundColor = 0x39BCA3;
		//Nubes
		h = window.innerHeight;
		w = window.innerWidth;

		max_alto =  h*0.68;
		max_ancho = w*0.25;

		


	},
	preload: function(){
		//Loading image
		
		game.load.image('logo','assets/images/logo.png');
		game.load.image('nube-1','assets/images/nube-1.png');
		game.load.image('nube-2','assets/images/nube-2.png');
		//
		game.load.image('joku','assets/images/spo_joku.png');
		game.load.image('ffrida','assets/images/spo_ff.png');
		game.load.image('moff','assets/images/spo_moff.png');
		game.load.image('pd','assets/images/spo_pd.png');
		game.load.image('sorte','assets/images/spo_sorte-logo.png');
		game.load.image('sag','assets/images/spo_sag.png');
		game.load.image('bandaids','assets/images/spo_bandaids.png');
		game.load.image('gm','assets/images/spo_gm-desing.png');
		game.load.image('mar','assets/images/spo_mar.png');
		game.load.image('indie-soma','assets/images/spo_indie-soma.png');
		game.load.image('salas','assets/images/spo_salasanimation.png');
		game.load.image('sueno','assets/images/spo_suenoconaruba.png');

		game.load.image('facebook','assets/images/redes-1.png');
		game.load.image('twitter','assets/images/redes-2.png');
		game.load.image('instagram','assets/images/redes-3.png');
		
		game.load.image('sp_cupid1','assets/images/sp_cupid1.png');
		game.load.image('sp_cupid2','assets/images/sp_cupid2.png');
		game.load.image('sp_cupid3','assets/images/sp_cupid3.png'); 
		game.load.image('sp_cupid4','assets/images/sp_cupid4.png');
		game.load.image('sp_cupid5','assets/images/sp_cupid5.png');
		game.load.image('sp_ovni','assets/images/sp_ovni.png');
		game.load.image('sp_cheo','assets/images/sp_cheo.png');
		game.load.image('sp_chuo','assets/images/sp_chuo.png');
		game.load.image('sp_camila','assets/images/sp_camila.png');
		game.load.image('sp_daniel','assets/images/sp_daniel.png');
		game.load.image('sp_rubi','assets/images/sp_rubi.png'); 
 

	},
	create: function(){
		cloud_layer = game.add.group();
		front_layer = game.add.group();

		c = 1;

		this.nube1 = cloud_layer.create(w*0.08,h*0.2,'nube-1');
		this.nube2 = cloud_layer.create(w*0.08,h*0.60,'nube-1');
		this.nube3 = cloud_layer.create(w*0.43,h*0.85,'nube-2');
		this.nube4 = cloud_layer.create(w*0.70,h*0.60,'nube-2');
		this.nube5 = cloud_layer.create(w*1.20,h*0.24,'nube-1');
		
		//Clouds Scales
		this.nube1.scale.setTo(0.25,0.25);
		
		this.nube2.scale.setTo(0.25,0.25);
		
		this.nube3.scale.setTo(0.2,0.2);
		
		this.nube4.scale.setTo(0.2,0.2);
		
		this.nube5.scale.setTo(0.25,0.25);
		
		this.nube5.scale.setTo(0.1,0.1);



		// Capa 2
		this.Title = game.make.text(w*0.68,w*0.10, "SPONSORS", textStyle);
		//this.logo = front_layer.create(w*0.78,h*0.85,'logo');
		//this.logo.scale.setTo(0.4,0.35);

		sponsor = front_layer.create(w*0.58,h*0.25,'moff');
			sponsor.width = w*0.20;
			sponsor.height = sponsor.width;
		//this.sponsor.scale.setTo(0.75,0.75);
		//this.sponsor.anchor.setTo(0.5);

		facebook = cloud_layer.create(w*0.65,h*0.74,'facebook');
		twitter = cloud_layer.create(w*0.675,h*0.74,'twitter');
		instagram = cloud_layer.create(w*0.7,h*0.74,'instagram');
		

		// name 
		this.name = game.make.text(w*0.68,h*0.7, "Moff Café", textStyle);
		front_layer.add(this.name);
		this.setClicks(sponsors[0]);

		// Big sprite
		bigSprite = front_layer.create(w*0.15,h*0.17,'sp_cupid2');
		bigSprite.height = max_alto;
		bigSprite.width = max_alto;

		//bigSprite.scale.setTo(0.9,0.9)
		// numero 2

		ref = this.name;

		//Text Anchors
		game.add.existing(this.Title).anchor.setTo(0.5);
		game.add.existing(this.name).anchor.setTo(0.5);


		//front_layer.bringToTop();

		
		game.stage.disableVisibilityChange = true;


		//Clouds Anchors

		game.add.existing(this.nube1).anchor.setTo(0.5);

		game.add.existing(this.nube2).anchor.setTo(0.5);

		game.add.existing(this.nube3).anchor.setTo(0.5);

		game.add.existing(this.nube4).anchor.setTo(0.5);

		game.add.existing(this.nube5).anchor.setTo(0.5);


		function randOrd(){
		  return (Math.round(Math.random())-0.5);
		}

			
		var exittext = game.make.text(window.innerWidth * 0.5, h * 0.90, 'ESPACIO PARA CONTINUAR',whiteStyle);
		front_layer.add(exittext);
		exittext.anchor.setTo(0.5);


	},
	setClicks: function(sponsor){
		facebook.inputEnabled = true;	
			twitter.inputEnabled = true;	
			instagram.inputEnabled = true;
		if(sponsor.instagram != undefined){
					var toInsta = function(){			
						window.open(sponsor.instagram,'1455458326347','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=0,left=0,top=0');					
					};

					instagram.events.onInputUp.add(toInsta);
				}else
					instagram.events.onInputUp.removeAll();
				
					
				if(sponsor.facebook != undefined){
					var toFace = function(){			
						window.open(sponsor.facebook,'1455458326347','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=0,left=0,top=0');					
					};
					facebook.events.onInputUp.add(toFace);

				}else
					facebook.events.onInputUp.removeAll();

				if(sponsor.twitter != undefined){
					var toTwitter = function(){			
					window.open(sponsor.twitter,'1455458326347','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=0,resizable=0,left=0,top=0');					
					};
					twitter.events.onInputUp.add(toTwitter);

				}else
					twitter.events.onInputUp.removeAll();

},
	update: function(){

		this.nube1.x += 1.2;
		this.nube2.x += 1.1;
		this.nube3.x += 1.09;
		this.nube4.x += 1.08;
		this.nube5.x += 0.9;

		this.screenWrap(this.nube1);
		this.screenWrap(this.nube2);
		this.screenWrap(this.nube3);
		this.screenWrap(this.nube4);
		this.screenWrap(this.nube5);
		
		if(game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && onebyone){
			if(c < sponsors.length){
			 	ref.setText(sponsors[c].name);
			 	bigSprite.loadTexture(lasimagenes[c]);
		 		sponsor.loadTexture(sponsors[c].picture);
		 		this.setClicks(sponsors[c]);
			}
			else{
				c = 0;
				this.game.stateTransition.to('GameMenu');
			}
		 	c++;
		 	onebyone=false;
		 	setTimeout(function(){ onebyone=true; }, 1000);
		}



	},
	screenWrap: function(sprite) {

    if (sprite.x < 0)
    {
        sprite.x = game.width;
    }
    else if (sprite.x > game.width)
    {
        sprite.x = 0;
    }

    if (sprite.y < 0)
    {
        sprite.y = game.height;
    }
    else if (sprite.y > game.height)
    {
        sprite.y = 0;
    }

}

};