var Phaser = Phaser || {};
var Platformer = Platformer || {};

var Options = function(game){};

Options.prototype ={
	
	menuConfig: {
	    className: "inverse",
	    startY: game.world.centerY + 80,
	    startX: "center"
	},

	init: function() {
		

		
		
		this.optionCount = 1;	
	},
	create: function(){
		//Declare options
		var playSound = gameOptions.playSound,
			playMusic = gameOptions.playMusic;


		// Se agrega un fondo
		var bg = game.add.sprite(0,0,'menu-bg');
		bg.width = window.innerWidth;
		bg.height = window.innerHeight;
		//Se agrega el titulo
		
		this.logo = game.make.sprite(game.world.centerX, game.world.centerY-75,  'brand');
		
		game.add.existing(this.logo).anchor.setTo(0.5);
		
		//Se agregan las opciones del menu
		// por funncion en mixins.js addMenuOption(label, callback, style);
		this.addMenuOption(gameOptions.playSound ? 'Sonidos: ON' : 'Sonidos: OFF', function(target){
			gameOptions.playSound = !gameOptions.playSound;
			target.text = gameOptions.playSound ? 'Sonidos: ON' : 'Sonidos: OFF';
		},"default");

		this.addMenuOption(gameOptions.playMusic ? 'Música: ON' : 'Música: OFF', function(target){
			gameOptions.playMusic = !gameOptions.playMusic;
			target.text = gameOptions.playMusic ? 'Música: ON' : 'Música: OFF';
			
			if(gameOptions.playMusic){
				musicPlayer.volume  = 0.3;
				musicPlayer.play();
			}else{
				musicPlayer.pause();
			}

		},"default");

		this.addMenuOption('Volver', function(){
			game.state.start("GameMenu");
		},"default");
	}
};

Phaser.Utils.mixinPrototype(Options.prototype, mixins);