var Phaser = Phaser || {};
var Platformer = Platformer || {};
var nlvlText;
var cityText;
var title;

Platformer.LoadingState = function () {
    "use strict";
    Phaser.State.call(this);
};

Platformer.prototype = Object.create(Phaser.State.prototype);
Platformer.prototype.constructor = Platformer.LoadingState;

Platformer.LoadingState.prototype.init = function (level_data) {
    "use strict";
    this.level_data = level_data;

};

Platformer.LoadingState.prototype.preload = function () {
    "use strict";
    var assets, asset_loader, asset_key, asset;

  


     nlvlText = game.make.text(window.innerWidth * 0.5 ,200, "LEVEL " + (levelCounter + 1), {
            font: 'bold 80pt JoyStix',
            fill: 'white',
            align: 'left'

        });

        nlvlText.fixedToCamera = true;
        var posY = nlvlText.height/4 + game.world.centerY;

        cityText = game.make.text(window.innerWidth * 0.5,posY, nameOfLevel[levelCounter], {
            font: 'bold 40pt JoyStix',
            fill: 'white',
            align: 'left'

        });
        cityText.fixedToCamera = true;
        //nlvlText.setShadow(3, 3, 'black', 5);
        this.optionCount = 1;

        
        game.stage.disableVisibilityChange = true;
        game.stage.backgroundColor = 0x000000;
        game.add.existing(nlvlText).anchor.setTo(0.5);
        game.add.existing(cityText).anchor.setTo(0.5);

         var posY = cityText.height + cityText.y + 80;

        title = game.make.text(window.innerWidth * 0.5,posY, "Cargando", {
            font: 'bold 20pt JoyStix',
            fill: 'white',
            align: 'left'

        });

         game.add.existing(title).anchor.setTo(0.5);

        
        function centerSpriteX(width){
            var tamano = window.innerWidth;
            var restante = tamano - width;
            var result = 0;
            if(restante>0){
                result = (restante/2);
            }
            return result;
        }

    assets = this.level_data.assets;
    for (asset_key in assets) { // load assets according to asset key
        if (assets.hasOwnProperty(asset_key)) {
            asset = assets[asset_key];
            switch (asset.type) {
            case "image":
                this.load.image(asset_key, asset.source);
                break;
            case "spritesheet":
                this.load.spritesheet(asset_key, asset.source, asset.frame_width, asset.frame_height, asset.frames, asset.margin, asset.spacing);
                break;
            case "tilemap":
                this.load.tilemap(asset_key, asset.source, null, Phaser.Tilemap.TILED_JSON);
                break;
            }
        }
    }
};

Platformer.LoadingState.prototype.create = function () {
    "use strict";
                //this.game.stateTransition.to('BootState', true, false, "assets/levels/level1.json");
    //Transicion
            this.game.stateTransition.configure({
              duration: Phaser.Timer.SECOND * 0.8,
              ease: Phaser.Easing.Exponential.InOut,
              properties: {
                alpha: 0,
                scale: {
                  x: 1.4,
                  y: 1.4
                }
              }
            });

    var ref = this.level_data;
    
    
        setTimeout(function(){

            killemall();
            title.kill();
            this.game.stateTransition.to("GameState", true, false, ref);

        },1000);

    function killemall(){
        nlvlText.kill();
        cityText.kill();
  
    }

};