var Phaser = Phaser || {};
var Platformer = Platformer || {};
var scorebylvl = [];
var timeleftbylvl = [];
var logox = 0;
var logoy = 0;
var levelCounter = 0;
var nameOfLevel = new Array('MARACAIBO', 'LOS ANDES', 'MOCHIMA','CARACAS', 'ESPACIO');
var levels = new Array('assets/levels/level1.json','assets/levels/level2.json','assets/levels/level3.json','assets/levels/level4.json','assets/levels/level5.json')
var timexstamp = Math.round(new Date() / 1000);

var 	Main = function(){},
	gameOptions = {
		playSound : true,
		playMusic : false //DIOS MIO CRISTO DESCANSE
	},
	musicPlayer;

var game = new Phaser.Game("100%", "100%", Phaser.CANVAS);


Main.prototype = {

	preload: function(){
		//loading images
		game.load.script('WebFont','js/vendor/webfontloader/webfontloader.js');
		game.load.image('bkground', 'assets/images/sky.png');
		game.load.image('loading', 'assets/images/preloader-bar.png');
		game.load.image('brand', 'assets/images/logo.png');
		game.load.image('lilbrand', 'assets/images/logolil.png');
		
		//loading scripts
		game.load.script('polyfill', 'js/libs/polyfill.js');
		game.load.script('utils', 'js/libs/utils.js');
		game.load.script('Splash', 'js/states/Splash.js?fuckcache='+timexstamp);

		WebFontConfig = {
			custom: {
				families: ['JoyStix'],
				urls: ['assets/style/joystixmonospace.css']
			}
		}

		
	},

	create: function(){
		game.state.add('Splash',Splash);
		game.state.start('Splash');
	}
};


game.state.add('Main', Main);
game.state.start('Main');